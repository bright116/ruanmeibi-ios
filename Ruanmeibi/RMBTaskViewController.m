//
//  FirstViewController.m
//  Ruanmeibi
//
//  Created by Liang Zhu on 1/16/14.
//  Copyright (c) 2014 Liang Zhu. All rights reserved.
//

#import "RMBTaskViewController.h"
#import "RMBTaskTableViewCell.h"
#import "RMBManager.h"
#import "RMBConfig.h"
#import "RMBUtils.h"
#import "RMBConsts.h"
#import "RMBOfferwall.h"
#import "RMBYoumiOfferWall.h"
#import "MBProgressHUD.h"
#import "SIAlertView.h"
#import "CircleLoadingView.h"
#import "HelpViewController.h"

@interface RMBTaskViewController ()

// Private Properties
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *pointsLabel;
@property (weak, nonatomic) IBOutlet UILabel *moneyLabel;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *debugButton;
@property (strong, nonatomic) SIAlertView *alertView;

@end

@implementation RMBTaskViewController

#pragma mark - UIViewController
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // 检测恶意用户
#ifndef DEBUG
    self.navigationItem.rightBarButtonItem = nil;

    if ([self detectSimulators]) {
        return;
    }

    if ([self detectJailbreakDevices]) {
        return;
    }
#endif
    
    //第一次启动新版本弹出新手教程
    NSString *version = [RMBUtils appVersion];
    NSString *lastVersion = [[NSUserDefaults standardUserDefaults] objectForKey:@"lastVersion"];
    if ([version floatValue] > [lastVersion floatValue]) {
        [[NSUserDefaults standardUserDefaults] setObject:version forKey:@"lastVersion"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self showHelpView];
    }
    
    //导航栏左侧添加反馈按钮
    UIBarButtonItem *t_leftButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"新手教程"
                                                                         style:UIBarButtonItemStylePlain
                                                                        target:self
                                                                        action:@selector(showHelpView)];

    self.navigationItem.leftBarButtonItem = t_leftButtonItem;

    // 与服务器端通信获得用户信息
    [[RMBManager sharedManager] createUser];
    
    self.alertView = [[SIAlertView alloc] init];
    
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    [[RMBManager sharedManager] addObserver:self
                                 forKeyPath:@"createUserLoadingStatus"
                                    options:NSKeyValueObservingOptionNew
                                    context:nil];
    
    [[RMBManager sharedManager] addObserver:self
                                 forKeyPath:@"getNewTaskLoadingStatus"
                                    options:NSKeyValueObservingOptionNew
                                    context:nil];
    
    
    [[RMBConfig sharedConfig] addObserver:self
                               forKeyPath:@"displayingOfferwalls"
                                  options:NSKeyValueObservingOptionNew|NSKeyValueObservingOptionOld
                                  context:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onAppEnterBackground)
                                                 name:UIApplicationWillResignActiveNotification
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onAppEnterForeground)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];
}

-(void)onAppEnterBackground
{
    [self.alertView dismissAnimated:NO];
}

-(void)onAppEnterForeground
{
    // 与服务器端通信获得用户信息
    [[RMBManager sharedManager] setAppEnterForeground:YES];
    [[RMBManager sharedManager] createUser];
}

- (void)viewWillAppear:(BOOL)animated
{
    self.title = @"赚金币";
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self.alertView dismissAnimated:NO];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
    [[RMBConfig sharedConfig] removeObserver:self forKeyPath:@"displayingOfferwalls"];
    [[RMBManager sharedManager] removeObserver:self forKeyPath:@"createUserLoadingStatus"];
    [[RMBManager sharedManager] removeObserver:self forKeyPath:@"getNewTaskLoadingStatus"];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[RMBConfig sharedConfig].displayingOfferwalls count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RMBTaskTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"OfferwallTaskCell" forIndexPath:indexPath];
    cell.backgroundColor = [UIColor whiteColor];

    NSArray *displayedAds = [RMBConfig sharedConfig].displayingOfferwalls;
    RMBOfferwall *selectedAds = displayedAds[indexPath.row];

    cell.offerwallVendorImageView.image = selectedAds.image;
    cell.offerwallVendorNameLabel.text = selectedAds.name;
    cell.offerwallVendorDescription.text = selectedAds.promotion;
    
    cell.offerwallVendorNameLabel.textColor = UIColorFromRGB(0x028cc1);
    cell.offerwallVendorDescription.textColor = UIColorFromRGB(0x414954);
    cell.userInteractionEnabled = YES;

    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DDLogVerbose(@"User selected row %d", (int)indexPath.row);
    
    // TODO: 动态检查用户当前的状态以及积分墙的被封锁情况

    NSArray *displayedAds = [RMBConfig sharedConfig].displayingOfferwalls;
    RMBOfferwall *selectedAds = displayedAds[indexPath.row];
    [selectedAds showOfferwallWithController:self];
}

#pragma mark - Value Observing Callbacks
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if([keyPath isEqualToString:@"displayingOfferwalls"]) {

        for (RMBOfferwall *offerwall in [RMBConfig sharedConfig].displayingOfferwalls) {
            [offerwall initializeOfferwall];
        }
        [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
    }
    else if ([keyPath isEqualToString:@"createUserLoadingStatus"]) {
        RMBNetworkLoadingStatus newValue = [change[NSKeyValueChangeNewKey] intValue];
        if (newValue == RMBNetworkLoadingStatusLoaded) {
            [self performSelectorOnMainThread:@selector(checkBlockedUser) withObject:nil waitUntilDone:YES];
            [self performSelectorOnMainThread:@selector(updateUserPointsLabel) withObject:nil waitUntilDone:YES];
        }
    }
    else if ([keyPath isEqualToString:@"getNewTaskLoadingStatus"]) {
        RMBNetworkLoadingStatus newValue = [change[NSKeyValueChangeNewKey] intValue];
        if (newValue == RMBNetworkLoadingStatusLoaded) {
            if ([[[RMBManager sharedManager] userNewTask][@"newTaskTotalPoints"] intValue] > 0) {
                [[RMBManager sharedManager] fetchUserExchangeHistory:0 pageCount:5 needsTotalCount:YES];
                [[RMBManager sharedManager] fetchUserTaskHistory:0 pageCount:5];
                [self showGetNewTask:[[RMBManager sharedManager] userNewTask][@"newTaskTotalPoints"]];
            }
        }
    }
}

-(void)updateUserPointsLabel
{
    int userPoints = [RMBManager sharedManager].user.userPoints;
    NSString *pointsString = [NSString stringWithFormat:@"%d", userPoints];
    NSString *moneyString = [NSString stringWithFormat:@"%.2f", userPoints / 100.0f];
    self.pointsLabel.text = pointsString;
    self.moneyLabel.text = moneyString;
}

#pragma mark - Private Methods
// 模拟器检查, 阻止模拟器用户登录
-(BOOL)detectSimulators
{
    // 开发模式下不需要检查的东西
#if DEBUG
    DDLogDebug(@"######################################");
    DDLogDebug(@"App Version : %@", [RMBUtils appVersion]);
    DDLogDebug(@"App Build   : %@", [RMBUtils appBuild]);
    DDLogDebug(@"Device Model: %@", [RMBUtils deviceModel]);
    DDLogDebug(@"Device OS   : %@", [RMBUtils deviceOS]);
    DDLogDebug(@"Device IDFA : %@", [RMBUtils deviceIDFA]);
    DDLogDebug(@"######################################");
#endif

    // 模拟器检测，不能强迫退出应用。
    // 原因参考：https://developer.apple.com/library/ios/qa/qa1561/_index.html
    if (![RMBUtils isRealDevice]) {
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:ALERT_SIMULATOR_TITLE
                                                         andMessage:ALERT_SIMULATOR_MESSAGE];
        alertView.backgroundStyle = SIAlertViewBackgroundStyleSolid;
        alertView.titleColor = [UIColor redColor];
        alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
        
        [alertView show];
        
        return true;
    } else {
        return false;
    }
}

// 越狱检查, 阻止越狱用户登录
-(BOOL)detectJailbreakDevices
{
    if ([RMBUtils isDeviceJailbroken]) {
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:ALERT_JAILBREAK_TITLE
                                                         andMessage:ALERT_JAILBREAK_MESSAGE];
        alertView.backgroundStyle = SIAlertViewBackgroundStyleSolid;
        alertView.titleColor = [UIColor redColor];
        alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
        
        [alertView show];
        
        return true;
    } else {
        return false;
    }
}

-(void)checkBlockedUser
{
    // check if user is blocked
    int blockedTime = [RMBManager sharedManager].user.time;
    if (blockedTime > 0) {
        // 用户被禁了
        self.alertView.title = @"登录失败";
        self.alertView.message = @"抱歉!服务器压力太大了, 请您稍后再试.";
        self.alertView.backgroundStyle = SIAlertViewBackgroundStyleSolid;
        self.alertView.titleColor = [UIColor redColor];
        self.alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
        
        [self.alertView show];
    } else if (blockedTime == -1) {
        self.alertView.title = @"登录失败";
        self.alertView.message = @"很遗憾!您的账号已被禁止登录.";
        self.alertView.backgroundStyle = SIAlertViewBackgroundStyleSolid;
        self.alertView.titleColor = [UIColor redColor];
        self.alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
        
        [self.alertView show];
    }
}

-(void)createInfoBarButton
{
    // TODO: remove info bar button before 1.0
    UIImage* infoIcon_normal = [UIImage imageNamed:@"info"];
    CGRect buttonFrame = CGRectMake(0, 0, infoIcon_normal.size.width, infoIcon_normal.size.height);
    
    UIButton *infoBarButton = [[UIButton alloc] initWithFrame:buttonFrame];
    [infoBarButton setBackgroundImage:infoIcon_normal forState:UIControlStateNormal];
    
    UIBarButtonItem *infoBarButtonItem =[[UIBarButtonItem alloc] initWithCustomView:infoBarButton];
    self.navigationItem.rightBarButtonItem = infoBarButtonItem;
}

-(void)showHelpView
{
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
    HelpViewController *helpViewCon = [[HelpViewController alloc] init];
    [self presentViewController:helpViewCon animated:NO completion:nil];
}

-(void)showGetNewTask:(NSString *)newTaskTotalPoints
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"恭喜您获得积分"
                                                    message:[NSString stringWithFormat:@"恭喜您刚刚获得了 %@ 积分", newTaskTotalPoints]
                                                   delegate:nil
                                          cancelButtonTitle:@"知道了"
                                          otherButtonTitles:nil,nil];
    [alert show];
}

@end
