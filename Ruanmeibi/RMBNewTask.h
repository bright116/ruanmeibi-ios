//
//  RMBNewTask.h
//  Ruanmeibi
//
//  Created by TonyLee on 14-5-19.
//  Copyright (c) 2014年 Liang Zhu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RMBNewTask : NSObject

@property (nonatomic, assign) int taskId;
@property (nonatomic, strong) NSString *appName;
@property (nonatomic, strong) NSString *vendorName;
@property (nonatomic, assign) int points;

@end
