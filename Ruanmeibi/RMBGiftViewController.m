//
//  SecondViewController.m
//  Ruanmeibi
//
//  Created by Liang Zhu on 1/16/14.
//  Copyright (c) 2014 Liang Zhu. All rights reserved.
//

#import "RMBGiftViewController.h"
#import "RMBGift.h"
#import "RMBGiftCollectionViewCell.h"
#import "RMBManager.h"
#import "RMBPhoneChargeDetailViewController.h"
#import "CircleLoadingView.h"

@interface RMBGiftViewController ()

@property (nonatomic, strong) RMBGift *selectedGift;
@property (strong, nonatomic) UIBarButtonItem *refreshButton;

@property (weak, nonatomic) IBOutlet UILabel *pointsLabel;
@property (weak, nonatomic) IBOutlet UILabel *moneyLabel;
@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;

- (IBAction)refreshButtonPressed:(UIBarButtonItem *)sender;



@end

@implementation RMBGiftViewController

#pragma mark - UIViewController Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.alwaysBounceVertical = YES;
    
    [self updateUserPointsLabel];
    
    [[RMBManager sharedManager] addObserver:self
                                 forKeyPath:@"createUserLoadingStatus"
                                    options:NSKeyValueObservingOptionNew
                                    context:NULL];
    
    [[RMBManager sharedManager] addObserver:self
                      forKeyPath:@"giftsLoadingStatus"
                         options:NSKeyValueObservingOptionNew|NSKeyValueObservingOptionOld
                         context:NULL];
}

-(void)viewWillAppear:(BOOL)animated {
    self.tabBarController.navigationItem.title = @"兑换";
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (void)dealloc {
    [[RMBManager sharedManager] removeObserver:self forKeyPath:@"giftsLoadingStatus"];
    [[RMBManager sharedManager] removeObserver:self forKeyPath:@"createUserLoadingStatus"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if ([RMBManager sharedManager].gifts == nil) {
        return 0;
    }
    
    return [[RMBManager sharedManager].gifts count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    RMBGiftCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Gift" forIndexPath:indexPath];
    
    RMBGift *gift = ([RMBManager sharedManager].gifts)[indexPath.row];
    cell.giftNameLabel.text = gift.giftName;
    cell.giftCost.text = [NSString stringWithFormat:@"%d金币", gift.giftCost];
    if (gift.giftType == 0) {
        UIImage *giftImage = [UIImage imageNamed:@"gift_phone_charge"];
        cell.giftImage.image = giftImage;
    } else {
        UIImage *giftImage = [UIImage imageNamed:@"gift_qq_charge"];
        cell.giftImage.image = giftImage;
    }
    
    cell.giftImage.layer.cornerRadius = 10.0f;
    cell.giftImage.clipsToBounds = YES;

    return cell;
}

#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    DDLogDebug(@"Selected Cell at %d", (int)indexPath.row);
    
    self.selectedGift = ([RMBManager sharedManager].gifts)[indexPath.row];
    
#ifndef DEBUG
    if ([RMBManager sharedManager].user.userPoints < self.selectedGift.giftCost) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"金币不够" message:@"同学你的金币不够哦, 努力再赚点金币吧~" delegate:Nil cancelButtonTitle:@"回去赚金币" otherButtonTitles:nil, nil];
        
        [alert show];
        return;
    }
#endif
    
    [self performSegueWithIdentifier:@"ExchangeDetailSegue" sender:self];
}

#pragma mark - Value Observing Callbacks
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if([keyPath isEqualToString:@"giftsLoadingStatus"]){
        RMBNetworkLoadingStatus oldValue = [change[NSKeyValueChangeOldKey] intValue];
        RMBNetworkLoadingStatus newValue = [change[NSKeyValueChangeNewKey] intValue];
        
        DDLogDebug(@"gifts changed from %d to %d", (int)oldValue, (int)newValue);
        
        [self.collectionView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
        
        // TODO: 错误状态下的提示
        // TODO: 保存上一次的兑换列表
    } else if ([keyPath isEqualToString:@"createUserLoadingStatus"]) {
        [self performSelectorOnMainThread:@selector(updateUserPointsLabel) withObject:nil waitUntilDone:YES];
    }
}

- (IBAction)refreshButtonPressed:(UIBarButtonItem *)sender {
    if ([RMBManager sharedManager].giftsLoadingStatus != RMBNetworkLoadingStatusLoading) {
        [[RMBManager sharedManager] fetchGifts];
    }
    DDLogDebug(@"refreshButtonPressed");
}

- (void)refreshGiftList {
    DDLogDebug(@"refreshGiftList triggered");
    
    if ([RMBManager sharedManager].giftsLoadingStatus != RMBNetworkLoadingStatusLoading) {
        [[RMBManager sharedManager] fetchGifts];
    }
    DDLogDebug(@"refreshButtonPressed");
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"ExchangeDetailSegue"]) {
        RMBPhoneChargeDetailViewController *destController = segue.destinationViewController;
        
        destController.userSelctedGift = self.selectedGift;
    }
}

-(void)updateUserPointsLabel
{
    int userPoints = [RMBManager sharedManager].user.userPoints;
    NSString *pointsString = [NSString stringWithFormat:@"%d", userPoints];
    NSString *moneyString = [NSString stringWithFormat:@"%.2f", userPoints / 100.0f];
    self.pointsLabel.text = pointsString;
    self.moneyLabel.text = moneyString;
}
@end
