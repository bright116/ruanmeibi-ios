//
//  RMBConfig.h
//  Ruanmeibi
//
//  Created by Liang Zhu on 14-2-18.
//  Copyright (c) 2014年 Liang Zhu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RMBConfig : NSObject

// 当前正在显示的积分墙列表
@property (nonatomic, strong) NSArray *displayingOfferwalls;

// 判断应用是否已经上线(也用于苹果审核状态判定)
@property (nonatomic) BOOL appReadyForSale;

// 判断应用是否为debug模式(开发调试时才用)
@property (nonatomic) BOOL appInDebug;

// 获取应用的配置信息(用于动态调整应用内容)
- (void)syncAppConfig;

// Singleton
+ (instancetype)sharedConfig;

@end
