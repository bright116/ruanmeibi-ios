//
//  AppDelegate.m
//  Ruanmeibi
//
//  Created by Liang Zhu on 1/16/14.
//  Copyright (c) 2014 Liang Zhu. All rights reserved.
//

#import "AppDelegate.h"
#import "RMBManager.h"
#import "RMBConfig.h"
#import "RMBUtils.h"
#import "DDASLLogger.h"
#import "DDTTYLogger.h"
#import "UIColor+FPBrandColor.h"
#import "Flurry.h"
#import "AFNetworkActivityIndicatorManager.h"
#import "MobClick.h"
#import "AarkiContact.h"
#import "Harpy.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // 初始化Log系统
    [DDLog addLogger:[DDASLLogger sharedInstance]];
    [DDLog addLogger:[DDTTYLogger sharedInstance]];
    [[DDTTYLogger sharedInstance] setColorsEnabled:YES];
    
    // 定制全局状态栏
    [UIApplication sharedApplication].statusBarHidden = NO;
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
    [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
    
    // 启动用户统计
    [Flurry startSession:FLURRY_APP_KEY];
    
    // 启动友盟统计
    [MobClick startWithAppkey:UMENG_APP_KEY reportPolicy:SEND_INTERVAL channelId:nil];
    [MobClick setAppVersion:[RMBUtils appVersion]];
    [MobClick setLogSendInterval:40];
    [MobClick setLogEnabled:NO];
    
    //版本更新检测
    // Set the App ID for your app
    [[Harpy sharedInstance] setAppID:APPID];
    
    // (Optional) Set the App Name for your app
    [[Harpy sharedInstance] setAppName:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"]];
    
    /* (Optional) Set the Alert Type for your app
     By default, the Singleton is initialized to HarpyAlertTypeOption */
    [[Harpy sharedInstance] setAlertType:HarpyAlertTypeSkip];
    
    /* (Optional) If your application is not availabe in the U.S. App Store, you must specify the two-letter
     country code for the region in which your applicaiton is available. */
    [[Harpy sharedInstance] setCountryCode:COUNTRYCODE];
    
    /* (Optional) Overides system language to predefined language.
     Please use the HarpyLanguage constants defined inHarpy.h. */
    [[Harpy sharedInstance] setForceLanguageLocalization:HarpyLanguageChineseSimplified];
    
    // Perform check for new version of your app
    [[Harpy sharedInstance] checkVersion];

    return YES;
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    DDLogVerbose(@"applicationDidBecomeActive");
    
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    //应用打开取消已注册本地推送
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [[RMBConfig sharedConfig] syncAppConfig];
    
    // TODO: to be removed and integrated into RMBOfferwall
    [AarkiContact registerApp:AARKI_CLIENT_SECURITY_KEY];
    
    
    /*
     Perform daily check for new version of your app
     Useful if user returns to you app from background after extended period of time
     Place in applicationDidBecomeActive:
     
     Also, performs version check on first launch.
     */
    [[Harpy sharedInstance] checkVersionDaily];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    DDLogVerbose(@"applicationWillEnterForeground");
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    DDLogVerbose(@"applicationWillResignActive");
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    DDLogVerbose(@"applicationDidEnterBackground");
    
    //应用后台时添加本地推送
    [self registerLocalNotification: 60 * 60 * 24];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    DDLogVerbose(@"applicationWillTerminate");
    
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - Private Methods

//注册本地推送
-(void)registerLocalNotification:(NSTimeInterval)time
{
    int userPoints = [RMBManager sharedManager].user.userPoints;
    NSString *alertBodyString;

    if (userPoints == 0) {
        alertBodyString = [NSString stringWithFormat:@"赶快回来赚金币吧,可以兑换话费和Q币哦!"];
    } else {
        alertBodyString = [NSString stringWithFormat:@"您还有%d金币,赶快回来兑换吧!", userPoints];
    }

    UILocalNotification *notification = [[UILocalNotification alloc] init];
    notification.fireDate = [[NSDate date] dateByAddingTimeInterval:time];
    notification.repeatInterval = NSCalendarUnitDay;
    notification.timeZone = [NSTimeZone defaultTimeZone];
    notification.soundName = UILocalNotificationDefaultSoundName;
    notification.applicationIconBadgeNumber = 1;
    notification.alertBody = alertBodyString;

    [[UIApplication sharedApplication] scheduleLocalNotification:notification];
}

@end
