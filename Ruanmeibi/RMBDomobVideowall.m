//
//  RMBDomobVideowall.m
//  Ruanmeibi
//
//  Created by Liang on 14-4-26.
//  Copyright (c) 2014年 Liang Zhu. All rights reserved.
//

#import "RMBDomobVideowall.h"

@interface RMBDomobVideowall ()

@property (nonatomic, strong) DMVideoViewController *videoWallController;

@end

@implementation RMBDomobVideowall

- (void)initializeOfferwall
{
    self.videoWallController = [[DMVideoViewController alloc] initWithPublisherID:kDomobOfferwallPublisherID];
}

- (void)showOfferwallWithController:(UIViewController *)controller
{
    [self.videoWallController presentVideoAdView];
}

-(void) applicationDidEnterBackground
{
    // default implementation does nothing
}

-(void) applicationDidEnterForeground
{
    // default implementation does nothing
}

//开始加载视频。
- (void)offerVideoDidStartLoad
{
    DDLogVerbose(@"%s", __PRETTY_FUNCTION__);
}

//积分视频⼲⼴广告加载完成。
- (void)offerVideoDidFinishLoad
{
    DDLogVerbose(@"%s", __PRETTY_FUNCTION__);
}

// 积分墙加载失败。可能的原因由 error 部分提供,例如⺴⽹网络连接失败、被禁⽤用等。
- (void)offerVideoDidFailLoadWithError:(NSError *)error
{
    DDLogVerbose(@"%s: %@", __PRETTY_FUNCTION__, [error localizedDescription]);
}

// 积分视频广告页面被关闭。
- (void)offerVideoDidClosed
{
    DDLogVerbose(@"%s", __PRETTY_FUNCTION__);
}

@end
