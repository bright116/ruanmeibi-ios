//
//  TaskTableViewCell.h
//  Ruanmeibi
//
//  Created by Liang Zhu on 14-1-22.
//  Copyright (c) 2014年 Liang Zhu. All rights reserved.
//

@interface RMBTaskTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *offerwallVendorImageView;
@property (weak, nonatomic) IBOutlet UILabel *offerwallVendorNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *offerwallVendorDescription;

@end