//
//  RMBUser.h
//  Ruanmeibi
//
//  Created by Liang Zhu on 14-1-22.
//  Copyright (c) 2014年 Liang Zhu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RMBUser : NSObject

@property (nonatomic, strong) NSString *userLoginId;
@property (nonatomic, strong) NSString *userFriendlyName;
@property (nonatomic) int userPoints;

@property (nonatomic) int time;
@property (nonatomic, strong) NSString *msg;

@end
