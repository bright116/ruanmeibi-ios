//
//  RMBDianruOfferwall.m
//  Ruanmeibi
//
//  Created by Liang on 14-3-29.
//  Copyright (c) 2014年 Liang Zhu. All rights reserved.
//

#import "RMBDianruOfferwall.h"
#import <AdSupport/ASIdentifierManager.h>

// Q1: 后台设置中的"回调密钥"是做什么用的, 文档里没有看到相关说明
// Q2: 是否支持IDFA, iOS7?
// Q3: 对于服务器回调, didReceiveSpendScoreResult/didReceiveGetScoreResult是否还有用?
// Q4: SDImage是怎么回事, 需要手动关闭ARC?

@implementation RMBDianruOfferwall

- (id)init
{
    self = [super init];
    if (self) {
    }
    
    return self;
}

// 初始化广告
- (void)initializeOfferwall
{
    [DianRuAdWall beforehandAdWallWithDianRuAppKey:kDianruAppKey];
}

- (void)showOfferwallWithController:(UIViewController *)controller
{
    [DianRuAdWall showAdWall:controller];
}

-(void) applicationDidEnterBackground
{
    [DianRuAdWall dianruOnPause];
}

-(void) applicationDidEnterForeground
{
    [DianRuAdWall dianruOnResume];
}
@end
