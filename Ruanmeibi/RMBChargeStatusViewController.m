//
//  RMBChargeStatusViewController.m
//  Ruanmeibi
//
//  Created by Liang on 14-3-28.
//  Copyright (c) 2014年 Liang Zhu. All rights reserved.
//

#import "RMBChargeStatusViewController.h"
#import "RMBManager.h"

@interface RMBChargeStatusViewController ()

@end

@implementation RMBChargeStatusViewController
- (IBAction)doneButtonTapped:(UIButton *)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
    [[RMBManager sharedManager] createUser];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.hidesBackButton = YES;
    
    int remainedCount = MAX_DAILY_EXCHANGE_COUNT - [RMBUtils readUserExchangeCountToday];
    self.remainingExchangeCountTodayLabel.text = [NSString stringWithFormat:@"%d", remainedCount];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
