//
//  RMBUserInfoViewController.h
//  Ruanmeibi
//
//  Created by Liang Zhu on 14-1-26.
//  Copyright (c) 2014年 Liang Zhu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UMFeedback.h"

@interface RMBUserInfoViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate>
@property(nonatomic, strong) UMFeedback *umFeedback;
@end
