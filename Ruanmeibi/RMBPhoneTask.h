//
//  RMBPhoneTask.h
//  Ruanmeibi
//
//  Created by Liang Zhu on 14-1-27.
//  Copyright (c) 2014年 Liang Zhu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RMBPhoneTask : NSObject
@property int taskId;
@property int taskType;
@property int taskVendorId;
@property NSString *taskAppName;
@property int taskPoints;
@property NSDate *timeStamp;

- (void)setTimeStampWithString:(NSString *)timestampString;

@end
