//
//  YoumiOfferWallVendor.m
//  Ruanmeibi
//
//  Created by Liang Zhu on 14-2-21.
//  Copyright (c) 2014年 Liang Zhu. All rights reserved.
//

#import "RMBYoumiOfferWall.h"
#import "YouMiWall.h"

@implementation RMBYoumiOfferWall

- (id)init
{
    self = [super init];
    if (self) {
    }
    
    return self;
}

// 初始化有米广告
- (void)initializeOfferwall
{
    [YouMiConfig setUseInAppStore:YES];
    [YouMiConfig launchWithAppID:kYouMiID appSecret:kYouMiSecret];
    [YouMiConfig setFullScreenWindow:[[[UIApplication sharedApplication] delegate] window]];
    [YouMiWall enable];
}

- (void)showOfferwallWithController:(UIViewController *)controller
{
    [YouMiWall showOffers:YES didShowBlock:^{
        NSLog(@"有米积分墙已显示");
    } didDismissBlock:^{
        NSLog(@"有米积分墙已退出");
    }];
}
@end
