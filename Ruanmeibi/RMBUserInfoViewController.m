//
//  RMBUserInfoViewController.m
//  Ruanmeibi
//
//  Created by Liang Zhu on 14-1-26.
//  Copyright (c) 2014年 Liang Zhu. All rights reserved.
//

#import "RMBUserInfoViewController.h"
#import "HMSegmentedControl.h"
#import "RMBManager.h"
#import "RMBUserRewardHistoryItemCell.h"
#import "RMBUserTaskHistoryItemCell.h"
#import "RMBReward.h"
#import "RMBPhoneTask.h"
#import "RMBGift.h"
#import "RMBUtils.h"
#import "CircleLoadingView.h"
#import <AdSupport/ASIdentifierManager.h>

#define SEG_BG_COLOR            UIColorFromRGB(0x872701)
#define SEG_SELECTED_BG_COLOR   UIColorFromRGB(0xff7743)

@interface RMBUserInfoViewController ()
@property (weak, nonatomic) IBOutlet UIView *segmentControlContainer;
@property (weak, nonatomic) IBOutlet UITableView *rewardHistoryTableView;
@property (weak, nonatomic) IBOutlet UITableView *taskHistoryTableView;
@property (weak, nonatomic) IBOutlet UIScrollView *segmentScrollView;
@property (weak, nonatomic) IBOutlet UILabel *userPointsLabel;
@property (weak, nonatomic) IBOutlet UILabel *userFriendlyNameLabel;
@property (weak, nonatomic) IBOutlet UIView *userInfoPanelContainerView;

@property (strong, nonatomic) CircleLoadingView *loadingView;
@property (strong, nonatomic) UIBarButtonItem *refreshButton;

@property (strong, nonatomic) HMSegmentedControl *segmentControl;

@property (strong, nonatomic) NSDictionary *taskVendorMap;

@end

@implementation RMBUserInfoViewController
@synthesize umFeedback = _umFeedback;

#pragma mark - UIViewController Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configSegmentControl];
    
    //导航栏左侧添加反馈按钮
    UIBarButtonItem *t_leftButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"反馈" style:UIBarButtonItemStylePlain target:self action:@selector(userFeedback)];
    self.navigationItem.leftBarButtonItem = t_leftButtonItem;
    
    // 创建LoadingView
    self.loadingView = [[CircleLoadingView alloc]initWithFrame:CGRectMake(0, 0, 25, 25)];
    self.refreshButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refreshButtonPressed:)];
    self.navigationItem.rightBarButtonItem = self.refreshButton;
    
    // 配置ScrollView
    [self.segmentScrollView setDelegate:self];
    
    self.userPointsLabel.text = [NSString stringWithFormat:@"%d", [RMBManager sharedManager].user.userPoints];
    self.userFriendlyNameLabel.text = [RMBManager sharedManager].user.userFriendlyName;
    
    self.rewardHistoryTableView.backgroundColor = [UIColor whiteColor];
    self.taskHistoryTableView.backgroundColor = [UIColor whiteColor];
    
    self.taskVendorMap = @{@0:@"有米",
                           @1:@"Aarki",
                           @2:@"米迪",
                           @3:@"点入",
                           @4:@"易积分",
                           @5:@"磨盘",
                           @6:@"多盟",
                           @99:@"赚金币"};
    
    [[RMBManager sharedManager] addObserver:self
                      forKeyPath:@"createUserLoadingStatus"
                         options:NSKeyValueObservingOptionNew
                         context:NULL];
   
    [[RMBManager sharedManager] addObserver:self
                      forKeyPath:@"taskHistoryLoadingStatus"
                         options:NSKeyValueObservingOptionNew
                         context:NULL];
    
    [[RMBManager sharedManager] addObserver:self
                      forKeyPath:@"rewardHistoryLoadingStatus"
                         options:NSKeyValueObservingOptionNew
                         context:NULL];
}

- (void)viewWillAppear:(BOOL)animated {
    self.tabBarController.navigationItem.title = @"账户";
}

- (void)viewDidAppear:(BOOL)animated {
    [self.segmentScrollView setContentSize:CGSizeMake(640, self.segmentScrollView.bounds.size.height)];
    [self.segmentScrollView scrollRectToVisible:CGRectMake(320 * self.segmentControl.selectedSegmentIndex, 0, 320, 1) animated:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    [[RMBManager sharedManager] removeObserver:self forKeyPath:@"createUserLoadingStatus"];
    [[RMBManager sharedManager] removeObserver:self forKeyPath:@"taskHistoryLoadingStatus"];
    [[RMBManager sharedManager] removeObserver:self forKeyPath:@"rewardHistoryLoadingStatus"];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    DDLogVerbose(@"numberOfRowsInSection returns %d", [[self dataSourceFromTableView:tableView] count]);
    return [[self dataSourceFromTableView:tableView] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DDLogVerbose(@"cellForRowAtIndexPath row=%d", indexPath.row);
    
    if (tableView == self.rewardHistoryTableView) {// 兑换历史
        static NSString *cellIdentifier1 = @"UserRewardHistoryCell";
    
        RMBUserRewardHistoryItemCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier1 forIndexPath:indexPath];
        
        cell.backgroundColor = (indexPath.row % 2 == 0) ? [UIColor whiteColor] : UIColorFromRGB(0xf4f4f4);
        cell.userInteractionEnabled = NO;
        
        RMBReward *exchange = ([RMBManager sharedManager].userExchangeHistory)[indexPath.row];
        
        cell.giftNameLabel.text = exchange.giftName;
        cell.exchangeCostLabel.text = [NSString stringWithFormat:@"-%d", exchange.cost];
        cell.exchangeStateLabel.text = [exchange getUserExchangeStatusString];
        cell.exchangeStartDateLabel.text = [RMBUtils formattedDateTimeString:exchange.timeStamp format:@"yyyy-MM-dd HH:mm"];

        if (exchange.userExchangeStatus == EXCHANGE_STATUS_ERROR) {
            cell.exchangeStateLabel.textColor = [UIColor redColor];
        } else if (exchange.userExchangeStatus == EXCHANGE_STATUS_DONE) {
            cell.exchangeStateLabel.textColor = [UIColor greenColor];
        } else {
            cell.exchangeStateLabel.textColor = [UIColor orangeColor];
        }
        
        return cell;
    }
    else { // 任务历史
        static NSString *cellIdentifier2 = @"UserTaskHistoryCell";
        
        RMBUserTaskHistoryItemCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier2 forIndexPath:indexPath];
        
        cell.backgroundColor = (indexPath.row % 2 == 0) ? [UIColor whiteColor] : UIColorFromRGB(0xf4f4f4);
        cell.userInteractionEnabled = NO;
        
        NSArray *userTaskHistory = [RMBManager sharedManager].userTaskHistory;
        RMBPhoneTask *task = userTaskHistory[indexPath.row];

        cell.taskVendorNameLabel.text = self.taskVendorMap[@(task.taskVendorId)];
        cell.taskEarnedPointsLabel.text = [NSString stringWithFormat:@"+%d", task.taskPoints];
        cell.taskAppNameLabel.text = task.taskAppName;
        cell.taskTimeStampLabel.text = [RMBUtils formattedDateTimeString:task.timeStamp format:@"yyyy-MM-dd HH:mm"];
        
        return cell;
    }    
}

- (RMBGift *)findGiftFromGifts:(NSArray *)gifts byGiftId:(int)giftId
{    
    for (RMBGift *gift in gifts) {
        if (gift.giftId == giftId) {
            return gift;
        }
    }
    
    return nil;
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if(scrollView == self.segmentScrollView) {
        CGFloat pageWidth = scrollView.frame.size.width;
        NSInteger page = scrollView.contentOffset.x / pageWidth;
        
        [self.segmentControl setSelectedSegmentIndex:page animated:YES];
    }
}

#pragma mark - Target Actions
- (void)segmentedControlChangedValue:(HMSegmentedControl *)segCtrl {
    [self.segmentScrollView scrollRectToVisible:CGRectMake(320 * segCtrl.selectedSegmentIndex, 0, 320, 1) animated:YES];
}

#pragma mark - Private Methods
- (NSArray *)dataSourceFromTableView: (UITableView *)tableView {
    if (tableView == self.rewardHistoryTableView) {
        return [RMBManager sharedManager].userExchangeHistory;
    } else if (tableView == self.taskHistoryTableView) {
        return [RMBManager sharedManager].userTaskHistory;
    }
    
    return nil;
}

- (void)refreshButtonPressed:(UIBarButtonItem *)sender {
    // 刷新按钮按下, 重新获得用户积分/任务历史/兑换历史
    [[RMBManager sharedManager] createUser];
    [[RMBManager sharedManager] fetchUserExchangeHistory:0 pageCount:5 needsTotalCount:YES];
    [[RMBManager sharedManager] fetchUserTaskHistory:0 pageCount:5];
}

//用户反馈
-(void)userFeedback
{
    NSString *idfa = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
    NSString *userLoginId = [RMBManager sharedManager].user.userLoginId;
    NSString *userFriendlyName = [RMBManager sharedManager].user.userFriendlyName;
    NSArray *userInfo = @[idfa, userLoginId, userFriendlyName];
    [UMFeedback showFeedback:self withAppkey:UMENG_APP_KEY dictionary:@{@"userInfo": userInfo}];
}

#pragma mark - Value Observing Callbacks
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if([keyPath isEqualToString:@"taskHistoryLoadingStatus"]){
        RMBNetworkLoadingStatus newValue = [change[NSKeyValueChangeNewKey] intValue];
        
        if(newValue == RMBNetworkLoadingStatusLoaded) {
            [self.taskHistoryTableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
        }
    }
    else if ([keyPath isEqualToString:@"rewardHistoryLoadingStatus"]){
        RMBNetworkLoadingStatus newValue = [change[NSKeyValueChangeNewKey] intValue];
        
        if(newValue == RMBNetworkLoadingStatusLoaded) {
            [self.rewardHistoryTableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
        }
    }
    else if ([keyPath isEqualToString:@"createUserLoadingStatus"]) {
        self.userPointsLabel.text = [NSString stringWithFormat:@"%d", [RMBManager sharedManager].user.userPoints];
        self.userFriendlyNameLabel.text = [RMBManager sharedManager].user.userFriendlyName;
    }
    
    if (   [RMBManager sharedManager].taskHistoryLoadingStatus != RMBNetworkLoadingStatusLoading
        &&([RMBManager sharedManager].rewardHistoryLoadingStatus != RMBNetworkLoadingStatusLoading)
        &&([RMBManager sharedManager].createUserLoadingStatus != RMBNetworkLoadingStatusLoading)) {

        [self performSelector:@selector(stopLoadingNavItem) withObject:nil afterDelay:1];
    }
    
    if (   [RMBManager sharedManager].taskHistoryLoadingStatus == RMBNetworkLoadingStatusLoading
        ||([RMBManager sharedManager].rewardHistoryLoadingStatus == RMBNetworkLoadingStatusLoading)
        ||([RMBManager sharedManager].createUserLoadingStatus == RMBNetworkLoadingStatusLoading)) {
        
        [self startLoadingNavItem];
    }
}

-(void)configSegmentControl
{
    HMSegmentedControl *segmentControl = [[HMSegmentedControl alloc] initWithFrame:CGRectMake(0, 0, 320, 30)];
    segmentControl.font = [UIFont boldSystemFontOfSize:14.0f];
    
    [segmentControl setSectionTitles:@[@"赚金币历史", @"兑换历史"]];
    [segmentControl setSelectionIndicatorHeight:4.0f];
    [segmentControl setBackgroundColor:SEG_BG_COLOR];
    [segmentControl setTextColor:[UIColor whiteColor]];
    [segmentControl setSelectedTextColor:[UIColor whiteColor]];
    [segmentControl setSelectionIndicatorColor:SEG_SELECTED_BG_COLOR];
    [segmentControl setSelectionStyle:HMSegmentedControlSelectionStyleBox];
    [segmentControl setSelectedSegmentIndex:0];
    [segmentControl setSelectionIndicatorLocation:HMSegmentedControlSelectionIndicatorLocationDown];
    [segmentControl setSegmentEdgeInset:UIEdgeInsetsMake(0, 6, 0, 6)];
    [segmentControl addTarget:self action:@selector(segmentedControlChangedValue:) forControlEvents:UIControlEventValueChanged];
    
    self.segmentControl = segmentControl;
    [self.segmentControlContainer addSubview:self.segmentControl];
}

-(void)startLoadingNavItem {
    UIBarButtonItem *loadingItem = [[UIBarButtonItem alloc]initWithCustomView:self.loadingView];
    self.navigationItem.rightBarButtonItem = loadingItem;
    [self.loadingView startAnimation];
}

-(void)stopLoadingNavItem {
    [self.loadingView stopAnimation];
    self.navigationItem.rightBarButtonItem = self.refreshButton;
}
@end
