//
//  RMBManager.m
//  Ruanmeibi
//
//  Created by Liang Zhu on 14-1-22.
//  Copyright (c) 2014年 Liang Zhu. All rights reserved.
//

#import "RMBManager.h"
#import "RMBGift.h"
#import "RMBReward.h"
#import "RMBPhoneTask.h"
#import "RMBNewTask.h"
#import "RMBHTTPJSONClient.h"
#import "AFHTTPRequestOperation.h"
#import "AFURLRequestSerialization.h"
#import <AdSupport/ASIdentifierManager.h>

#define DEFAULT_SERVER @"berlusconi.sinaapp.com";
#define DEFAULT_PORT @"80";
#define TASK_HISTORY_PAGE_SIZE          5
#define EXCHANGE_HISTORY_PAGE_SIZE      5

// 广告平台编号与名称映射
@interface RMBManager ()

@end

@implementation RMBManager

+ (instancetype)sharedManager
{
    static id _sharedInstace = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstace = [[self alloc] init];
    });
    
    return _sharedInstace;
}

- (id)init
{
    if (self = [super init]) {
        self.idfa = [[ASIdentifierManager sharedManager] advertisingIdentifier];
        self.client = [[RMBHTTPJSONClient alloc] init];
        
        self.appEnterForeground = NO;
        self.ipAddress = DEFAULT_SERVER;
        self.port = DEFAULT_PORT;
        [self setServerIP:self.ipAddress andPort:self.port];
        
        self.createUserLoadingStatus = RMBNetworkLoadingStatusUnloaded;
        self.giftsLoadingStatus = RMBNetworkLoadingStatusUnloaded;
        self.createUserExchangeTradeStatus = RMBNetworkLoadingStatusUnloaded;
        self.taskHistoryLoadingStatus = RMBNetworkLoadingStatusUnloaded;
        self.rewardHistoryLoadingStatus = RMBNetworkLoadingStatusUnloaded;
        self.getNewTaskLoadingStatus = RMBNetworkLoadingStatusUnloaded;
        
        self.userExchangeHistory = [NSMutableArray array];
        self.userTaskHistory = [NSMutableArray array];
        
        [self addObserver:self
               forKeyPath:@"createUserLoadingStatus"
                  options:NSKeyValueObservingOptionNew
                  context:NULL];
    }
    
    return self;
}

- (void)dealloc
{
    [self removeObserver:self forKeyPath:@"user"];
}

- (void)createUser
{
    
    if (self.createUserLoadingStatus == RMBNetworkLoadingStatusLoading) {
        return;
    }

    [self.client createUserWithIDFA:self.idfa
        success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject) { // 创建用户成功
            if (nil != responseObject) {
                DDLogDebug(@"创建用户成功! \n%@", responseObject);
                
                RMBUser *newUser = [[RMBUser alloc] init];
                newUser.userLoginId = [NSString stringWithFormat:@"%d", [responseObject[@"userLoginId"] intValue]];
                newUser.userFriendlyName = responseObject[@"friendlyName"];
                newUser.userPoints = [responseObject[@"userPoints"] intValue];
                newUser.time = [responseObject[@"time"] intValue];
                newUser.msg = responseObject[@"msg"];

                self.user = newUser;
                
                self.createUserLoadingStatus = RMBNetworkLoadingStatusLoaded;
            } else {
                DDLogError(@"创建用户响应解析失败!\n%@", operation.responseString);
                self.createUserLoadingStatus = RMBNetworkLoadingStatusError;
            }
        }
        failure:^(AFHTTPRequestOperation *operation, NSError *error) { // 创建用户失败
            self.createUserLoadingStatus = RMBNetworkLoadingStatusError;
            DDLogError(@"创建用户失败!\n%@", error);
            DDLogDebug(@"Response String\n%@", operation.responseString);
        }
     ];
    
    self.createUserLoadingStatus = RMBNetworkLoadingStatusLoading;
}

- (void)getUserStatus
{
    // 防止重复调用
    if (self.getUserStatusLoadingStatus == RMBNetworkLoadingStatusLoading) {
        return;
    }
    
    [self.client getUserStatusWithUserLoginId:self.user.userLoginId
        success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject) {
            if (nil != responseObject) {
                DDLogDebug(@"获取用户状态信息请求成功! \n%@", responseObject);
                int blocked_time = [responseObject[@"blocked_time"] intValue];
                self.user.time = blocked_time;
                
                self.getUserStatusLoadingStatus = RMBNetworkLoadingStatusLoaded;
            } else {
                DDLogError(@"获取用户状态信息解析失败!\n%@", operation.responseString);
                self.getUserStatusLoadingStatus = RMBNetworkLoadingStatusError;
            }
        }
        failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            DDLogError(@"获取用户状态信息失败!\n%@", error);
            DDLogDebug(@"Response String\n%@", operation.responseString);
            self.getUserStatusLoadingStatus = RMBNetworkLoadingStatusError;
        }
     ];
    
    self.getUserStatusLoadingStatus = RMBNetworkLoadingStatusLoading;
}

- (void)fetchGifts
{
    if (self.giftsLoadingStatus == RMBNetworkLoadingStatusLoading) {
        return;
    }
    
    [self.client fetchGiftsOfType:RMBGiftTypeAll
        success:^(AFHTTPRequestOperation *operation, NSArray *responseObject) { // 获取兑换物品列表成功
          if (nil != responseObject) {
              DDLogDebug(@"获取兑换物品列表请求成功! \n%@", responseObject);
              NSMutableArray *newGifts = [NSMutableArray array];
              
              NSArray *listJson = (NSArray *)responseObject;
              [listJson enumerateObjectsUsingBlock:^(NSDictionary *giftJson, NSUInteger idx, BOOL *stop) {
                  RMBGift *newGift = [[RMBGift alloc] init];
                  
                  newGift.giftId = [giftJson[@"id"] intValue];
                  newGift.giftType = [giftJson[@"type"] intValue];
                  newGift.giftName = giftJson[@"name"];
                  newGift.giftCost = [giftJson[@"pointsCost"] intValue];
                  newGift.giftExchangeRate = [giftJson[@"exchangeRate"] floatValue];
                  newGift.giftCounts = [giftJson[@"counts"] intValue];
                  
                  [newGifts addObject:newGift];
              }];
              
              self.gifts = newGifts;
              self.giftsLoadingStatus = RMBNetworkLoadingStatusLoaded;
          } else {
              DDLogError(@"获取兑换物品列表解析失败!\n%@", operation.responseString);
              self.giftsLoadingStatus = RMBNetworkLoadingStatusError;
          }
        }
        failure:^(AFHTTPRequestOperation *operation, NSError *error) { // 获取兑换物品列表失败
            DDLogError(@"获取兑换物品列表失败!\n%@", error);
            DDLogDebug(@"Response String\n%@", operation.responseString);
            self.giftsLoadingStatus = RMBNetworkLoadingStatusError;
        }
    ];
    
    self.giftsLoadingStatus = RMBNetworkLoadingStatusLoading;
}

- (void)createGiftExchangeWithGiftId:(int)giftId andPhoneNumber:(NSString *)userEnteredNumber
{
    if (self.rewardHistoryLoadingStatus == RMBNetworkLoadingStatusLoading) {
        return;
    }
    
    [self.client createExchangeWithUserLoginId:self.user.userLoginId
                                        giftId:giftId
                                  exchangeInfo:userEnteredNumber
                                       success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (nil != responseObject) {
            if([responseObject[@"userExchangeTradeId"] intValue] == 0) {
                DDLogDebug(@"创建兑换请求失败! \n%@", responseObject);
                self.createUserExchangeTradeStatus = RMBNetworkLoadingStatusError;
                return;
            }

            DDLogDebug(@"创建兑换请求成功! \n%@", responseObject);
            int spendingTradeId = [responseObject[@"userExchangeTradeId"] intValue];
            DDLogVerbose(@"userExchangeTradeId=%d", spendingTradeId);
            
            self.createUserExchangeTradeStatus = RMBNetworkLoadingStatusLoaded;
        } else {
            DDLogError(@"创建兑换响应解析失败!\n%@", operation.responseString);
            self.createUserExchangeTradeStatus = RMBNetworkLoadingStatusError;
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogError(@"创建兑换请求失败!\n%@", error);
        DDLogDebug(@"Response String\n%@", operation.responseString);
        
        // TODO: 需要告知用户
        self.createUserExchangeTradeStatus = RMBNetworkLoadingStatusError;
    }];
    
    self.createUserExchangeTradeStatus = RMBNetworkLoadingStatusLoading;
}

- (void)fetchUserExchangeHistory:(int)page pageCount:(int)pageCount needsTotalCount: (BOOL)needsTotalCount {
    if (self.rewardHistoryLoadingStatus == RMBNetworkLoadingStatusLoading) {
        return;
    }
    
    [self.client fetchUserExchangeHistoryWithUserLoginId:self.user.userLoginId page:page pageCount:pageCount needsTotalCount:needsTotalCount
    success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (nil != responseObject) {
            DDLogError(@"获取用户兑换历史请求成功!\n%@", responseObject);
            
            NSMutableArray *newRewardsList = [NSMutableArray array];
            
            NSArray *listJson = responseObject[@"rewards"];
            [listJson enumerateObjectsUsingBlock:^(NSDictionary *rewardJson, NSUInteger idx, BOOL *stop) {
                
                int userExchangeTradeId = [rewardJson[@"userExchangeTradeId"] intValue];
                int userExchangeStatus = [rewardJson[@"status"] intValue];
                int giftId = [rewardJson[@"giftId"] intValue];
                int cost = [rewardJson[@"cost"] intValue];
                
                NSString *giftName = rewardJson[@"giftName"];
                NSString *rewardNumber = rewardJson[@"rewardNumber"];
                NSString *timeStampString = rewardJson[@"timestamp"];
                
                RMBReward *reward = [[RMBReward alloc] init];
                reward.giftId = giftId;
                reward.giftName = giftName;
                reward.userExchangeTradeId = userExchangeTradeId;
                reward.userExchangeStatus = userExchangeStatus;
                reward.rewardNumber = [rewardNumber copy];
                reward.cost = cost;
                [reward setTimeStampWithString:timeStampString];
                                
                [newRewardsList addObject:reward];
                
            }];
            
            self.userExchangeHistory = newRewardsList;
            self.rewardHistoryLoadingStatus = RMBNetworkLoadingStatusLoaded;
        } else {
            DDLogError(@"获取用户兑换历史请求解析失败!\n%@", operation.responseString);
            self.rewardHistoryLoadingStatus = RMBNetworkLoadingStatusError;
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogError(@"获取用户兑换历史请求失败!\n%@", error);
        DDLogDebug(@"Response String\n%@", operation.responseString);
        self.rewardHistoryLoadingStatus = RMBNetworkLoadingStatusError;
    }];
    
    self.rewardHistoryLoadingStatus = RMBNetworkLoadingStatusLoading;
}

- (void)fetchUserTaskHistory:(int)page pageCount:(int)pageCount {
    if (self.taskHistoryLoadingStatus == RMBNetworkLoadingStatusLoading) {
        return;
    }
    
    [self.client fetchUserTaskHistoryWithUserLoginId:self.user.userLoginId page:page pageCount:pageCount
    success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (nil != responseObject) {
            DDLogError(@"获取用户任务历史请求成功!\n%@", responseObject);
            
            NSMutableArray *newTaskList = [NSMutableArray array];
            
            NSArray *listJson = responseObject[@"tasks"];
            [listJson enumerateObjectsUsingBlock:^(NSDictionary *taskJson, NSUInteger idx, BOOL *stop) {
                
                int taskId = [taskJson[@"userTaskId"] intValue];
                int taskType = [taskJson[@"type"] intValue];
                int taskVendorId = [taskJson[@"vendorId"] intValue];
                int taskPoints = [taskJson[@"points"] intValue];
                NSString *appName = taskJson[@"appName"];
                NSString *timeStampString = taskJson[@"timeStamp"];
                
                RMBPhoneTask *task = [[RMBPhoneTask alloc] init];
                task.taskId = taskId;
                task.taskType = taskType;
                task.taskVendorId = taskVendorId;
                task.taskPoints = taskPoints;
                task.taskAppName = appName;
                [task setTimeStampWithString:timeStampString];
                
                [newTaskList addObject:task];
                
            }];
            
            self.userTaskHistory = newTaskList;
            self.taskHistoryLoadingStatus = RMBNetworkLoadingStatusLoaded;
        } else {
            DDLogError(@"获取用户任务历史请求解析失败!\n%@", operation.responseString);
            self.taskHistoryLoadingStatus = RMBNetworkLoadingStatusError;
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DDLogError(@"获取用户任务历史请求失败!\n%@", error);
        DDLogDebug(@"Response String\n%@", operation.responseString);
        self.taskHistoryLoadingStatus = RMBNetworkLoadingStatusError;
    }];
    
    self.taskHistoryLoadingStatus = RMBNetworkLoadingStatusLoading;
}

- (void)getNewTask
{
    // 防止重复调用
    if (self.getNewTaskLoadingStatus == RMBNetworkLoadingStatusLoading) {
        return;
    }
    
    [self.client getUserNewTaskWithUserLoginId:self.user.userLoginId
                                      success:^(AFHTTPRequestOperation *operation, NSDictionary *responseObject) {
                                          if (nil != responseObject) {
                                              DDLogDebug(@"获取用户新完成任务信息请求成功! \n%@", responseObject);
                                              NSMutableDictionary *newTaskDic = [NSMutableDictionary dictionary];
                                              
                                              NSMutableArray *newTaskList = [NSMutableArray array];
                                              
                                              NSArray *listJson = responseObject[@"tasks"];
                                              [listJson enumerateObjectsUsingBlock:^(NSDictionary *taskJson, NSUInteger idx, BOOL *stop) {
                                                  
                                                  int taskId = [taskJson[@"id"] intValue];
                                                  int points = [taskJson[@"points"] intValue];
                                                  NSString *vendorNameString = taskJson[@"vendorName"];
                                                  NSString *appNameString = taskJson[@"appName"];
                                                  
                                                  RMBNewTask *task = [[RMBNewTask alloc] init];
                                                  task.taskId = taskId;
                                                  task.points = points;
                                                  task.vendorName = vendorNameString;
                                                  task.appName = appNameString;
                                                  
                                                  [newTaskList addObject:task];
                                                  
                                              }];
                                              NSString *newTaskTotalPoints =[NSString stringWithFormat:@"%@",responseObject[@"newTaskTotalPoints"]];
                                              
                                              [newTaskDic setObject:newTaskTotalPoints forKey:@"newTaskTotalPoints"];
                                              [newTaskDic setObject:newTaskList forKey:@"tasks"];
                                              
                                              self.userNewTask = newTaskDic;
                                              
                                              self.getNewTaskLoadingStatus = RMBNetworkLoadingStatusLoaded;
                                          } else {
                                              DDLogError(@"获取用户新完成任务信息解析失败!\n%@", operation.responseString);
                                              self.getNewTaskLoadingStatus = RMBNetworkLoadingStatusError;
                                          }
                                      }
                                      failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          DDLogError(@"获取用户新完成任务信息失败!\n%@", error);
                                          DDLogDebug(@"Response String\n%@", operation.responseString);
                                          self.getNewTaskLoadingStatus = RMBNetworkLoadingStatusError;
                                      }
     ];
    
    self.getNewTaskLoadingStatus = RMBNetworkLoadingStatusLoading;
}

#pragma mark - Value Observing Callbacks
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if([keyPath isEqualToString:@"createUserLoadingStatus"]){
        //判断条件需要加上状态，否则后台切前台会执行2次
        RMBNetworkLoadingStatus newValue = [change[NSKeyValueChangeNewKey] intValue];
        if (newValue == RMBNetworkLoadingStatusLoaded && self.user.userLoginId != NULL
            && ![self.user.userLoginId isEqualToString:@""]
            && self.user.time == 0) {
            [self getNewTask];
            if (!self.appEnterForeground) {
                [self fetchGifts];
                [self fetchUserExchangeHistory:0 pageCount:EXCHANGE_HISTORY_PAGE_SIZE needsTotalCount:YES];
                [self fetchUserTaskHistory:0 pageCount:TASK_HISTORY_PAGE_SIZE];
            }
            
        } else {
            // userLoginId没有得到
            DDLogError(@"userLoginId没有得到");
        }
    }
}

- (void)setServerIP:(NSString *)ipAddress andPort:(NSString *)port
{
    self.ipAddress = ipAddress;
    self.port = port;
    
    self.client.baseUrl = [NSString stringWithFormat:@"http://%@:%@/api/v1", ipAddress, port];
}

@end