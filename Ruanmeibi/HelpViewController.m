//
//  HelpViewController.m
//  whoTalking
//
//  Created by  on 12-3-6.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "HelpViewController.h"
#define MAXIMAGENUMBER 3

@implementation HelpViewController
@synthesize myScrollView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    self.myScrollView = [[UIScrollView alloc] initWithFrame:self.view.frame];
    self.myScrollView.backgroundColor = [UIColor blackColor];
    self.myScrollView.delegate = self;
    self.myScrollView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:myScrollView];
    
//    self.myPageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(140, self.view.frame.size.height-40, 40, 37)];
//    self.myPageControl.numberOfPages = MAXIMAGENUMBER;
//    self.myPageControl.currentPage = 0;
//    [self.view addSubview:self.myPageControl];
    
    for(int i = 0 ; i < MAXIMAGENUMBER; i++)
    {
        UIImageView *imageV = nil;
        //根据屏幕尺寸加载相应的图片
        if (self.view.frame.size.height == 568.0) {
            imageV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"help0%d-568h@2x.png", i+1]]];
        }else
        {
            imageV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"help0%d@2x.png", i+1]]];
        }

        imageV.backgroundColor = [UIColor whiteColor];
        [imageV setFrame:CGRectMake( i * 320, 0, 320, self.view.frame.size.height)];
        [myScrollView addSubview:imageV];
        if (i == MAXIMAGENUMBER - 1) {
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            btn.frame = CGRectMake(i * 320 + 85, self.view.frame.size.height-80, 150, 40);
            btn.backgroundColor = [UIColor colorWithRed:38.0/255.0 green:90.0/255.0 blue:198.0/255.0 alpha:1.0];
            [btn setTitle:@"开始体验" forState:UIControlStateNormal];
            //                [btn setBackgroundImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"bt_kaishi" ofType:@"png"]] forState:UIControlStateNormal];
            [btn addTarget:self action:@selector(clickCloseButton:) forControlEvents:UIControlEventTouchUpInside];
            [myScrollView addSubview:btn];
        }
    }
    [myScrollView setContentSize:CGSizeMake(MAXIMAGENUMBER * 320, self.view.frame.size.height)];
    [myScrollView setPagingEnabled:YES];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)clickCloseButton:(id)sender
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
    [self dismissViewControllerAnimated:NO completion:nil];
}

//-(void)scrollViewDidScroll:(UIScrollView *)scrollView
//{
//
//    CGFloat pageWidth = scrollView.frame.size.width;
//    int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
//    self.myPageControl.currentPage = page;
//}

-(void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
{
    float fIndex = fabs(myScrollView.contentOffset.x) / myScrollView.frame.size.width ;
    if(fIndex > MAXIMAGENUMBER - 1)
    {
        [self clickCloseButton:nil];
    }
    
}
@end
