//
//  RMBYijifenOfferwall.m
//  Ruanmeibi
//
//  Created by Liang on 14-4-25.
//  Copyright (c) 2014年 Liang Zhu. All rights reserved.
//

#import "RMBYijifenOfferwall.h"
#import<Escore/YJFUserMessage.h>
#import<Escore/YJFInitServer.h>
#import<Escore/YJFIntegralWall.h>

@implementation RMBYijifenOfferwall

- (id)init
{
    self = [super init];
    if (self) {
    }
    
    return self;
}

// 初始化广告
- (void)initializeOfferwall
{
    [YJFUserMessage shareInstance].yjfUserAppId = kYJFAppID; //应用ID
    [YJFUserMessage shareInstance].yjfUserDevId = kYJFDevID; //开发者ID
    [YJFUserMessage shareInstance].yjfAppKey = kYJFAppKey;   //appKey
    [YJFUserMessage shareInstance].yjfChannel = @"IOS1.2.4"; //市场渠道号
    
    [YJFUserMessage shareInstance].yjfCoop_info = kYJFAppID;
    
    YJFInitServer *InitData = [[YJFInitServer alloc]init];
    [InitData getInitEscoreData];
}

- (void)showOfferwallWithController:(UIViewController *)controller
{
    YJFIntegralWall *integralWall = [[YJFIntegralWall alloc]init];

    [controller presentViewController:integralWall animated:YES completion:nil];
}

-(void) applicationDidEnterBackground
{

}

-(void) applicationDidEnterForeground
{

}

@end
