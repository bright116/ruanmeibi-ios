//
//  RMBUserTaskHistoryItemCell.m
//  Ruanmeibi
//
//  Created by Liang Zhu on 14-1-27.
//  Copyright (c) 2014年 Liang Zhu. All rights reserved.
//

#import "RMBUserTaskHistoryItemCell.h"

@implementation RMBUserTaskHistoryItemCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
