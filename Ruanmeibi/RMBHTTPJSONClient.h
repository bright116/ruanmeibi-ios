//
//  RMBHTTPJSONClient.h
//  Ruanmeibi
//
//  Created by Liang Zhu on 14-1-21.
//  Copyright (c) 2014年 Liang Zhu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFURLRequestSerialization.h"
#import "AFHTTPRequestOperationManager.h"

@interface RMBHTTPJSONClient : NSObject

@property (nonatomic, strong) NSString *baseUrl;

typedef NS_ENUM(NSInteger, RMBGiftType) {
    RMBGiftTypeAll          = -1,
    RMBGiftTypePhoneCharge  = 0,
    RMBGiftTypeQQCharge     = 1,
};

// 创建新用户或者获取用户信息
- (void)createUserWithIDFA:(NSUUID *)idfa
                   success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                   failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

// 获取用户状态信息(黑名单，被封时间等等)
- (void)getUserStatusWithUserLoginId:(NSString *)userLoginId
                             success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                             failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

// 获取兑换物品
- (void)fetchGiftsOfType:(RMBGiftType)type
                 success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                 failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

// 创建用户兑换(话费、Q币等等)
- (void)createExchangeWithUserLoginId:(NSString *)userLoginId
                               giftId:(int)giftId
                         exchangeInfo:(NSString *)phoneNumber
                              success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                              failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

- (void)fetchUserExchangeHistoryWithUserLoginId:(NSString *)userLoginId
                                           page:(int)page
                                      pageCount:(int)pageCount
                                needsTotalCount:(BOOL) needsTotalCount
                                        success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                                        failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

- (void)fetchUserTaskHistoryWithUserLoginId:(NSString *)userLoginId
                                           page:(int)page
                                      pageCount:(int)pageCount
                                        success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                                        failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;
//获取新的完成任务总分和信息列表
-(void)getUserNewTaskWithUserLoginId:(NSString *)userLoginId
                             success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                             failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;
@end
