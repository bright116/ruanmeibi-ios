//
//  RMBHTTPJSONClient.m
//  Ruanmeibi
//
//  Created by Liang Zhu on 14-1-21.
//  Copyright (c) 2014年 Liang Zhu. All rights reserved.
//

#import "RMBHTTPJSONClient.h"

#define CREATE_USER_URL                     @"%@/user/ios/create.json"
#define GET_USER_STATUS_URL                 @"%@/user/%@/getStatus.json?sig=%@"

#define CREATE_USER_PHONE_EXCHANGE_URL      @"%@/reward/%@/phone.json"
#define GET_REWARD_HISTROY_URL              @"%@/reward/%@/all/history.json?page=%d&count=%d&needsTotalCount=%@"
#define GET_TASK_HISTROY_URL                @"%@/task/%@/all/history.json?page=%d&count=%d"

//#define FETCH_GIFTS_URL                     @"%@/gifts.json"
#define FETCH_GIFTS_URL                     @"http://gctquiz.sinaapp.com/Zhuanjinbi_gifts_v040.json"
#define GET_NEW_TASK_URL                    @"%@/task/%@/getNewTask.json"

@interface RMBHTTPJSONClient ()
@property (nonatomic, strong) AFHTTPRequestOperationManager *requestManager;
@end

@implementation RMBHTTPJSONClient

- (void)createUserWithIDFA:(NSUUID *)idfa
                   success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                   failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    // 尝试创建新用户
    NSString *idfaString = [idfa UUIDString];
    NSString *signature = [RMBHTTPJSONClient generateSignature];
    
    // 产生请求体内容
    NSDictionary *iosDevice = @{@"idfa": idfaString};
    NSDictionary *newUser = @{@"signature" : signature, @"device" : iosDevice};
    NSString *url = [NSString stringWithFormat:CREATE_USER_URL, self.baseUrl];
    
    AFJSONRequestSerializer *requestSerializer = [AFJSONRequestSerializer serializer];
    AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializer];
    NSMutableURLRequest *postRequest = [requestSerializer requestWithMethod:@"POST" URLString:url parameters:newUser error:nil];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:postRequest];
    
    [operation setResponseSerializer:responseSerializer];
    [operation setCompletionBlockWithSuccess:success failure:failure];
    [operation start];
    
    DDLogDebug(@"发送创建用户请求...\n%@", newUser);
}

- (void)getUserStatusWithUserLoginId:(NSString *)userLoginId
                             success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                             failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSString *signature = [RMBHTTPJSONClient generateSignature];
    NSString *getUserStatusUrl = [NSString stringWithFormat:GET_USER_STATUS_URL, self.baseUrl, userLoginId, signature];
    
    AFJSONRequestSerializer *requestSerializer = [AFJSONRequestSerializer serializer];
    AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializer];
    
    NSURLRequest *request = [requestSerializer requestWithMethod:@"GET" URLString:getUserStatusUrl parameters:nil error:nil];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setResponseSerializer:responseSerializer];
    [operation setCompletionBlockWithSuccess:success failure:failure];
    [operation start];
    
    DDLogDebug(@"获取用户状态...\n");
}

- (void)fetchGiftsOfType:(RMBGiftType)type
                 success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                 failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    AFJSONRequestSerializer *requestSerializer = [AFJSONRequestSerializer serializer];
    AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializer];

    NSString *fetchGiftsUrl = [NSString stringWithFormat:FETCH_GIFTS_URL];
//    NSString *fetchGiftsUrl = [NSString stringWithFormat:FETCH_GIFTS_URL, self.baseUrl];
    NSURLRequest *request = [requestSerializer requestWithMethod:@"GET" URLString:fetchGiftsUrl parameters:nil error:nil];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setResponseSerializer:responseSerializer];
    [operation setCompletionBlockWithSuccess:success failure:failure];
    [operation start];
    
    DDLogDebug(@"获取兑换物品列表...\n");
}

- (void)createExchangeWithUserLoginId:(NSString *)userLoginId
                               giftId:(int)giftId
                         exchangeInfo:(NSString *)phoneNumber
                              success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                              failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    // 尝试创建请求URL
    NSString *createUserPhoneExcangeURL = [NSString stringWithFormat:CREATE_USER_PHONE_EXCHANGE_URL, self.baseUrl, userLoginId];
    NSString *signature = [RMBHTTPJSONClient generateSignature];
    
    // 产生请求体内容
    NSDictionary *newUserPhoneExchange = @{@"signature"      : signature,
                                           @"giftId"         : [NSNumber numberWithInt:giftId],
                                           @"phoneNumber"    : phoneNumber};
    
    AFJSONRequestSerializer *requestSerializer = [AFJSONRequestSerializer serializer];
    AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializer];
    NSURLRequest *postRequest = [requestSerializer requestWithMethod:@"POST" URLString:createUserPhoneExcangeURL parameters:newUserPhoneExchange error:nil];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:postRequest];
    
    [operation setResponseSerializer:responseSerializer];
    [operation setCompletionBlockWithSuccess:success failure:failure];
    [operation start];
    
    DDLogDebug(@"发送创建用户兑换请求...\n%@", newUserPhoneExchange);
}

- (void)fetchUserExchangeHistoryWithUserLoginId:(NSString *)userLoginId
                                           page:(int)page pageCount:(int)pageCount
                                needsTotalCount: (BOOL) needsTotalCount
                                        success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                                        failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure {
    // 尝试创建请求URL
    NSString *url = [NSString stringWithFormat:GET_REWARD_HISTROY_URL, self.baseUrl, userLoginId, page, pageCount, ((needsTotalCount) ? @"true" : @"false")];
    
    AFJSONRequestSerializer *requestSerializer = [AFJSONRequestSerializer serializer];
    AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializer];
    NSURLRequest *request = [requestSerializer requestWithMethod:@"GET" URLString:url parameters:nil error:nil];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setResponseSerializer:responseSerializer];
    [operation setCompletionBlockWithSuccess:success failure:failure];
    [operation start];
    
    DDLogDebug(@"发送获取用户兑换历史请求...\n%@", url);
}

- (void)fetchUserTaskHistoryWithUserLoginId:(NSString *)userLoginId
                                       page:(int)page
                                  pageCount:(int)pageCount
                                    success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                                    failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure {
    // 尝试创建请求URL
    NSString *url = [NSString stringWithFormat:GET_TASK_HISTROY_URL, self.baseUrl, userLoginId, page, pageCount];
    
    AFJSONRequestSerializer *requestSerializer = [AFJSONRequestSerializer serializer];
    AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializer];
    NSURLRequest *request = [requestSerializer requestWithMethod:@"GET" URLString:url parameters:nil error:nil];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [operation setResponseSerializer:responseSerializer];
    [operation setCompletionBlockWithSuccess:success failure:failure];
    [operation start];
    
    DDLogDebug(@"发送获取用户已完成任务历史请求...\n%@", url);
}

- (void)getUserNewTaskWithUserLoginId:(NSString *)userLoginId
                             success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))success
                             failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSString *getUserNewTaskUrl = [NSString stringWithFormat:GET_NEW_TASK_URL, self.baseUrl, userLoginId];
    
    AFJSONRequestSerializer *requestSerializer = [AFJSONRequestSerializer serializer];
    AFJSONResponseSerializer *responseSerializer = [AFJSONResponseSerializer serializer];
    
    NSURLRequest *request = [requestSerializer requestWithMethod:@"GET" URLString:getUserNewTaskUrl parameters:nil error:nil];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setResponseSerializer:responseSerializer];
    [operation setCompletionBlockWithSuccess:success failure:failure];
    [operation start];
    
    DDLogDebug(@"获取用户新的完成任务总分和信息列表...\n");
}

// TODO: 产生signature
+ (NSString *)generateSignature {
    return @"xsdsdsdsdxxx";
}
@end
