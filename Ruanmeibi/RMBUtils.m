//
//  RMBUtils.m
//  Ruanmeibi
//
//  Created by Liang on 14-3-27.
//  Copyright (c) 2014年 Liang Zhu. All rights reserved.
//

#import <ifaddrs.h>
#import <arpa/inet.h>
#import <AdSupport/ASIdentifierManager.h>
#import "RMBUtils.h"
#import "MobClick.h"

@interface RMBUtils ()
+(BOOL)hasCydia;
+(BOOL)hasBashAccess;
@end

@implementation RMBUtils

// 检查应用是否是在真实设备上运行，还是在模拟器上运行
+(BOOL)isRealDevice {
    
    BOOL isRunningOnRealDevice = NO;
    
    // 运行时刻检查应用是否运行在模拟器上
    NSString *model = [RMBUtils deviceModel];
    if ([model isEqualToString:@"iPhone Simulator"]) {
        return false;
    }
    
#if TARGET_IPHONE_SIMULATOR
    isRunningOnRealDevice = NO;
#else
    isRunningOnRealDevice = YES;
#endif
    
    return isRunningOnRealDevice;
}

// Cydia is installed
// Verify some of the system paths
// Perform a sandbox integrity check
// Perform symlink verification
// Verify whether you create and write files outside your Sandbox
+(BOOL)isDeviceJailbroken
{
//#ifndef DEBUG
    // 越狱设备或者模拟器会允许bash访问
    if ([RMBUtils hasBashAccess]) {
        return YES;
    }
//#endif

    // TODO: Detect Access outside Sandbox
    return [RMBUtils hasCydia] || [MobClick isJailbroken];
}

+(BOOL)hasCydia
{
    // 大部分越狱设备安装了Cydia
    NSString *filePath = @"/Applications/Cydia.app";
    return ([[NSFileManager defaultManager] fileExistsAtPath:filePath]);
}

+(BOOL)hasBashAccess
{
    FILE *fileHanle = fopen("/bin/bash", "r");
    BOOL bashAccepted = (fileHanle != NULL);
    fclose(fileHanle);

    return bashAccepted;
}

+(NSString *)deviceIDFA
{
    NSUUID *idfa = [[ASIdentifierManager sharedManager] advertisingIdentifier];
    return [idfa UUIDString];
}

+(NSString *)deviceModel
{
    return [[UIDevice currentDevice] model];
}

+(NSString *)deviceOS
{
    UIDevice *currentDevice = [UIDevice currentDevice];
    return [NSString stringWithFormat:@"%@ %@", currentDevice.systemName, currentDevice.systemVersion];
}

+(NSString *)appVersion
{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
}

+(NSString *)appBuild
{
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey];
}

+ (BOOL)isValidateMobile:(NSString *)mobile
{
    //手机号以13, 15, 18开头，八个 \d 数字字符
    NSString *phoneRegex = @"^((13[0-9])|(15[^4,\\D])|(18[0,0-9]))\\d{8}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    
    DDLogVerbose(@"phoneTest is %@",phoneTest);
    return [phoneTest evaluateWithObject:mobile];
}

/*
 * 验证QQ号码：
 * 1. 经典号码长度是6-10位，必须是数字
 * 2. 第一位不能为0
 * 3. 手机号也可以是QQ号了
 */
+ (BOOL)isValidateQQ:(NSString *)qq
{
    if (qq.length < 6 || qq.length > 11) {
        return NO;
    } else if ([[qq substringToIndex:1] isEqualToString:@"0"]) {
        return NO;
    }

    return YES;
}

#pragma mark DEPRECATED methods
+(NSString *)deviceLocalIPAddress
{
    NSString *address = @"error";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while (temp_addr != NULL) {
            if( temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if ([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                }
            }
            
            temp_addr = temp_addr->ifa_next;
        }
    }
    
    // Free memory
    freeifaddrs(interfaces);
    
    return address;
}

+ (NSString *)formattedDateTimeString:(NSDate *)date format:(NSString *)format
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    
    return [dateFormatter stringFromDate:date];
}

+(BOOL)isSameDay:(NSDate *)date1 withDate:(NSDate *)date2
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSUInteger dateComponentsFlags = NSMonthCalendarUnit|NSYearCalendarUnit|NSDayCalendarUnit;
    
    NSDateComponents *componentsDate1 = [calendar components:dateComponentsFlags fromDate:date1];
    NSDateComponents *componentsDate2 = [calendar components:dateComponentsFlags fromDate:date2];
    if (   ([componentsDate1 year] == [componentsDate2 year])
        && ([componentsDate1 month] == [componentsDate2 month])
        && ([componentsDate1 day] == [componentsDate2 day])) {
        // 年月日相同
        return YES;
    } else {
        return NO;
    }
}

+(int)readUserExchangeCountToday
{
    // 读取用户的兑换次数
    NSDate *today = [NSDate date];
    NSDate *userLatestExchangeDate = (NSDate *)[[NSUserDefaults standardUserDefaults] objectForKey:kLocal_Key_LatestExchangeDate];
    
    if (userLatestExchangeDate == nil) {
        return 0;
    } else {
        if ([RMBUtils isSameDay:today withDate:userLatestExchangeDate]) {
            return [[[NSUserDefaults standardUserDefaults] objectForKey:kLocal_Key_DailyExchangeCount] intValue];
        } else {
            return 0;
        }
    }
}

+(void)increaseUserExchangeCountToday
{
    int userExchangeCountToday = [self readUserExchangeCountToday];
    
    userExchangeCountToday++;
    NSNumber *exchangeCountObject = [NSNumber numberWithInt:userExchangeCountToday];
    
    NSDate *today = [NSDate date];
    [[NSUserDefaults standardUserDefaults] setObject:today forKey:kLocal_Key_LatestExchangeDate];
    [[NSUserDefaults standardUserDefaults] setObject:exchangeCountObject forKey:kLocal_Key_DailyExchangeCount];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
@end