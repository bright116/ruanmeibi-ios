//
//  MiidiOfferwall.m
//  Ruanmeibi
//
//  Created by Liang on 14-3-29.
//  Copyright (c) 2014年 Liang Zhu. All rights reserved.
//

#import "RMBMiidiOfferwall.h"
#import "MiidiManager.h"
#import "MiidiAdWall.h"
#import <AdSupport/ASIdentifierManager.h>

@implementation RMBMiidiOfferwall

- (id)init
{
    self = [super init];
    if (self) {
        self._id = 0;
        self.image = [UIImage imageNamed:@"aarki"];
        self.name = @"米迪积分墙入口";
        self.promotion = @"最快5分钟赚2元";
    }
    
    return self;
}

// 初始化米迪积分墙
- (void)initializeOfferwall
{
    NSString *uuidString = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
    
    [MiidiManager setAppPublisher:kMiidiID withAppSecret:kMiidiSecret];
    [MiidiAdWall setUserParam:uuidString];
}

- (void)showOfferwallWithController:(UIViewController *)controller
{
    [MiidiAdWall showAppOffers:controller withDelegate:self];
}

#pragma mark - MiidiAdWallShowAppOffersDelegate
// 请求应用列表成功
// 详解:广告墙请求成功后回调该方法
- (void)didReceiveOffers
{
    DDLogInfo(@"%s", __PRETTY_FUNCTION__);
}

// 请求应用列表失败
// 详解:广告墙请求失败后回调该方法
- (void)didFailToReceiveOffers:(NSError *)error
{
    DDLogInfo(@"%s: %@", __PRETTY_FUNCTION__, [error localizedDescription]);
}

#pragma mark Screen View Notification Methods

// 显示全屏页面
// 详解: 全屏页面显示完成后回调该方法
- (void)didShowWallView
{
    DDLogInfo(@"%s", __PRETTY_FUNCTION__);
}

// 隐藏全屏页面
// 详解:全屏页面隐藏完成后回调该方法
- (void)didDismissWallView
{
    DDLogInfo(@"didDismissWallView");
}

@end
