//
//  DebugSettingsViewController.m
//  Ruanmeibi
//
//  Created by Liang on 14-4-30.
//  Copyright (c) 2014年 Liang Zhu. All rights reserved.
//

#import "DebugSettingsViewController.h"
#import "SIAlertView.h"
#import "RMBManager.h"

@interface DebugSettingsViewController ()
@property (weak, nonatomic) IBOutlet UITextField *ipAddressLabel;
@property (weak, nonatomic) IBOutlet UITextField *portLabel;

@end

@implementation DebugSettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.ipAddressLabel.text = [RMBManager sharedManager].ipAddress;
    self.portLabel.text =[RMBManager sharedManager].port;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

+ (BOOL)isValidatIP:(NSString *)ipAddress {
    if (ipAddress == nil || [ipAddress isEqualToString:@""]) {
        return NO;
    }
    
    NSString  *urlRegEx =@"^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
    "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
    "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
    "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";
    
    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
    
    return [urlTest evaluateWithObject:ipAddress];
}

- (IBAction)saveButtonPressed:(UIButton *)sender {

    [[RMBManager sharedManager] setServerIP:self.ipAddressLabel.text andPort:self.portLabel.text];
    
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"设置成功"
                                                     andMessage:@"新的服务器地址和端口已设置"];
    [[RMBManager sharedManager] createUser];
    alertView.backgroundStyle = SIAlertViewBackgroundStyleSolid;
    alertView.titleColor = [UIColor greenColor];
    alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
    [alertView addButtonWithTitle:@"COOL"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alert) {
                              NSLog(@"Button1 Clicked");
                          }];
    
    [alertView show];

}

- (IBAction)resetButtonPressed:(UIButton *)sender {
    self.ipAddressLabel.text = @"berlusconi.sinaapp.com";
    self.portLabel.text = @"80";
    
    [[RMBManager sharedManager] setServerIP:self.ipAddressLabel.text
                                    andPort:self.portLabel.text];
}


@end
