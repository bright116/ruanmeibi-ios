//
//  MiidiOfferwall.h
//  Ruanmeibi
//
//  Created by Liang on 14-3-29.
//  Copyright (c) 2014年 Liang Zhu. All rights reserved.
//

#import "RMBOfferwall.h"
#import "MiidiAdWallShowAppOffersDelegate.h"

@interface RMBMiidiOfferwall : RMBOfferwall <MiidiAdWallShowAppOffersDelegate>

@end
