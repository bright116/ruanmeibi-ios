//
//  Offerwall.h
//  Ruanmeibi
//
//  Created by Liang Zhu on 14-2-21.
//  Copyright (c) 2014年 Liang Zhu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RMBOfferwall : NSObject
@property (nonatomic) int _id;
@property (nonatomic, strong) NSString *identifier;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *description;
@property (nonatomic, strong) NSString *promotion;
@property (nonatomic, strong) UIImage *image;


// 初始化广告
- (void)initializeOfferwall;

// 展示广告
- (void)showOfferwallWithController:(UIViewController *)controller;

//
-(void) applicationDidEnterBackground;
-(void) applicationDidEnterForeground;

@end
