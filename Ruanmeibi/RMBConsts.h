//
//  RMBConsts.h
//  Ruanmeibi
//
//  Created by Liang on 14-3-29.
//  Copyright (c) 2014年 Liang Zhu. All rights reserved.
//

#import <Foundation/Foundation.h>


FOUNDATION_EXPORT NSString * const ALERT_JAILBREAK_TITLE;
FOUNDATION_EXPORT NSString * const ALERT_JAILBREAK_MESSAGE;
FOUNDATION_EXPORT NSString * const ALERT_SIMULATOR_TITLE;
FOUNDATION_EXPORT NSString * const ALERT_SIMULATOR_MESSAGE;

FOUNDATION_EXPORT NSString * const ALERT_EXCEED_DAILY_EXCHANGE_COUNT_TITLE;
FOUNDATION_EXPORT NSString * const ALERT_EXCEED_DAILY_EXCHANGE_COUNT_MESSAGE;

@interface RMBConsts : NSObject

@end
