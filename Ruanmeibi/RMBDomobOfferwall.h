//
//  RMBDomobOfferwall.h
//  Ruanmeibi
//
//  Created by Liang on 14-4-26.
//  Copyright (c) 2014年 Liang Zhu. All rights reserved.
//

#import "RMBOfferwall.h"
#import "DMOfferWallViewController.h"

@interface RMBDomobOfferwall : RMBOfferwall <DMOfferWallDelegate>

@end
