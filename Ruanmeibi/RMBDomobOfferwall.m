//
//  RMBDomobOfferwall.m
//  Ruanmeibi
//
//  Created by Liang on 14-4-26.
//  Copyright (c) 2014年 Liang Zhu. All rights reserved.
//

#import "RMBDomobOfferwall.h"

@interface RMBDomobOfferwall ()

@property (nonatomic, strong) DMOfferWallViewController *offerWallController;

@end

@implementation RMBDomobOfferwall

- (void)initializeOfferwall
{
    
}

- (void)showOfferwallWithController:(UIViewController *)controller
{
    self.offerWallController = [[DMOfferWallViewController alloc] initWithPublisherID:kDomobOfferwallPublisherID];
    self.offerWallController.delegate = self;
    [self.offerWallController presentOfferWall];
}

-(void) applicationDidEnterBackground
{
    // default implementation does nothing
}

-(void) applicationDidEnterForeground
{
    // default implementation does nothing
}

// 积分墙开始加载列表数据。
- (void)offerWallDidStartLoad
{
    DDLogVerbose(@"%s", __PRETTY_FUNCTION__);
}

// 积分墙加载完成。
- (void)offerWallDidFinishLoad
{
    DDLogVerbose(@"%s", __PRETTY_FUNCTION__);
}

// 积分墙加载失败。可能的原因由error部分提供，例如网络连接失败、被禁用等。
- (void)offerWallDidFailLoadWithError:(NSError *)error
{
    DDLogVerbose(@"%s: %@", __PRETTY_FUNCTION__, [error localizedDescription]);
}

// 积分墙页面被关闭。
- (void)offerWallDidClosed
{
    DDLogVerbose(@"%s", __PRETTY_FUNCTION__);
}

@end
