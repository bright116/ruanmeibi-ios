//
//  RMBReward.h
//  Ruanmeibi
//
//  Created by Liang Zhu on 14-1-26.
//  Copyright (c) 2014年 Liang Zhu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RMBReward : NSObject

@property int userExchangeTradeId;
@property int giftId;
@property (nonatomic, strong) NSString *giftName;
@property int userExchangeStatus;
@property int cost;
@property (nonatomic, strong) NSString *rewardNumber;
@property (nonatomic, strong) NSDate *timeStamp;

- (void)setTimeStampWithString:(NSString *)timestampString;

- (NSString *)getUserExchangeStatusString;
@end
