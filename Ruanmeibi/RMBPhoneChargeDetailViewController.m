//
//  RMBPhoneChargeDetailViewController.m
//  Ruanmeibi
//
//  Created by Liang Zhu on 14-1-24.
//  Copyright (c) 2014年 Liang Zhu. All rights reserved.
//

#import "SIAlertView.h"
#import "RMBPhoneChargeDetailViewController.h"
#import "RMBManager.h"
#import "RMBUtils.h"
#import "MBProgressHUD.h"
#import <unistd.h>

@interface RMBPhoneChargeDetailViewController ()

@property (weak, nonatomic) IBOutlet UILabel *exchangeCostLabel;
@property (weak, nonatomic) IBOutlet UILabel *requestedGiftLabel;
@property (weak, nonatomic) IBOutlet UILabel *numberTypeLabel;

@property (weak, nonatomic) IBOutlet UITextField *phoneNumberToChargeTextField;
@property (weak, nonatomic) IBOutlet UIButton *confirmButton;

@property (weak, nonatomic) IBOutlet UILabel *pointsLabel;
@property (weak, nonatomic) IBOutlet UILabel *moneyLabel;

-(IBAction)confirmExchangePressed:(UIButton *)sender;

@property (strong, nonatomic) MBProgressHUD *hud;

@end

@implementation RMBPhoneChargeDetailViewController

#pragma mark - UIViewController Life Cycle
-(void)viewDidLoad
{
    [super viewDidLoad];
    
    self.phoneNumberToChargeTextField.delegate = self;

    self.hud = [[MBProgressHUD alloc] initWithView:self.view];
	[self.view addSubview:self.hud];

    if (self.userSelctedGift.giftType == RMBGiftTypePhoneCharge) {
        self.numberTypeLabel.text = @"手机号码:";
        self.title = @"手机充值";
    } else if (self.userSelctedGift.giftType == RMBGiftTypeQQCharge) {
        self.numberTypeLabel.text = @"QQ号码:";
        self.title = @"Q币充值";
    } else {
        self.phoneNumberToChargeTextField.enabled = NO;
        self.numberTypeLabel.text = @"未知充值类型:";
    }

    self.exchangeCostLabel.text = [NSString stringWithFormat:@"%d", self.userSelctedGift.giftCost];
    self.requestedGiftLabel.text = self.userSelctedGift.giftName;
    
    [self updateUserPointsLabel];
    
    // 定制确认兑换按钮的皮肤
    [self.confirmButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateDisabled];
    [self.confirmButton setBackgroundImage:[self imageWithColor:UIColorFromRGB(0x777777)] forState:UIControlStateDisabled];
    
    [self.confirmButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.confirmButton setBackgroundImage:[self imageWithColor:UIColorFromRGB(0x028cc1)] forState:UIControlStateNormal];
    self.confirmButton.layer.cornerRadius = 5.0f;
    self.confirmButton.clipsToBounds = YES;
    
    self.confirmButton.enabled = NO;
    
    [self.phoneNumberToChargeTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    [[RMBManager sharedManager] addObserver:self
                                 forKeyPath:@"createUserLoadingStatus"
                                    options:NSKeyValueObservingOptionNew
                                    context:NULL];
    
    [[RMBManager sharedManager] addObserver:self
                                 forKeyPath:@"createUserExchangeTradeStatus"
                                    options:NSKeyValueObservingOptionNew|NSKeyValueObservingOptionOld
                                    context:NULL];
    
}

- (UIImage *)imageWithColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

-(void)textFieldDidChange:(UITextField *)textField
{
    DDLogVerbose(@"textFieldDidChange>>>>%@", textField.text);
    NSString *phoneNumber = [NSString stringWithFormat:@"%@", textField.text];

    if (self.userSelctedGift.giftType == RMBGiftTypePhoneCharge) {
        self.confirmButton.enabled = [RMBUtils isValidateMobile:phoneNumber];
    } else if (self.userSelctedGift.giftType == RMBGiftTypeQQCharge) {
        self.confirmButton.enabled = [RMBUtils isValidateQQ:phoneNumber];
    } else {
        self.confirmButton.enabled = NO;
    }
}

-(void) viewDidAppear:(BOOL)animated
{
    // 自动弹起键盘
    [self.phoneNumberToChargeTextField becomeFirstResponder];
}

-(void)dealloc
{
    [[RMBManager sharedManager] removeObserver:self forKeyPath:@"createUserExchangeTradeStatus"];
    [[RMBManager sharedManager] removeObserver:self forKeyPath:@"createUserLoadingStatus"];
}

#pragma mark - UITextFieldDelegate
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.phoneNumberToChargeTextField resignFirstResponder];
}

- (IBAction)confirmExchangePressed:(UIButton *)sender
{
    // 检查用户当天的兑换次数
    int userExchangeCountToday = [RMBUtils readUserExchangeCountToday];
    if (userExchangeCountToday >= MAX_DAILY_EXCHANGE_COUNT) {
        // 告知用户超过当日兑换总数
        NSString *alertBody = [NSString stringWithFormat:ALERT_EXCEED_DAILY_EXCHANGE_COUNT_MESSAGE, MAX_DAILY_EXCHANGE_COUNT, userExchangeCountToday];
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:ALERT_EXCEED_DAILY_EXCHANGE_COUNT_TITLE
                                                         andMessage:alertBody];
        alertView.backgroundStyle = SIAlertViewBackgroundStyleSolid;
        alertView.titleColor = [UIColor redColor];
        alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
        [alertView addButtonWithTitle:@"知道了" type:SIAlertViewButtonTypeDestructive handler:^(SIAlertView *alert) {
        }];

        
        [alertView show];
        return;
    }
    
    [RMBManager sharedManager].createUserExchangeTradeStatus = RMBNetworkLoadingStatusUnloaded;
    [self.phoneNumberToChargeTextField resignFirstResponder];
    
    NSString *userEnteredNumber = self.phoneNumberToChargeTextField.text;
    
    // TODO: 将来需要将用户兑换, Q币兑换和手机充值抽象成模型对象
    if (self.userSelctedGift.giftType == RMBGiftTypePhoneCharge) {// 手机充值
        if (![RMBUtils isValidateMobile:userEnteredNumber]) {
            // 手机号码不正确
            return;
        }
    } else if (self.userSelctedGift.giftType == RMBGiftTypeQQCharge) {
        if (![RMBUtils isValidateQQ:userEnteredNumber]) {
            // QQ号码不正确
            return;
        }
    }
    
    self.hud.labelText = @"发送兑换请求...";
    self.hud.mode = MBProgressHUDModeIndeterminate;

    [self.hud show:YES];
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        [[RMBManager sharedManager] createGiftExchangeWithGiftId:self.userSelctedGift.giftId andPhoneNumber:userEnteredNumber];
        sleep(3);
//        [RMBManager sharedManager].createUserExchangeTradeStatus = RMBNetworkLoadingStatusLoading;
    });
}

#pragma mark - Value Observing Callbacks
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if([keyPath isEqualToString:@"createUserExchangeTradeStatus"]){
        RMBNetworkLoadingStatus oldValue = [change[NSKeyValueChangeOldKey] intValue];
        RMBNetworkLoadingStatus newValue = [change[NSKeyValueChangeNewKey] intValue];
        
        DDLogDebug(@"gifts changed from %d to %d", (int)oldValue, (int)newValue);
        
        if(oldValue == RMBNetworkLoadingStatusLoading) {
            
            if (newValue == RMBNetworkLoadingStatusError) {
                self.hud.labelText = @"发送失败! 请您稍后重试.";
                self.hud.mode = MBProgressHUDModeText;
                [self.hud hide:YES afterDelay:3];
            }
            else if (newValue == RMBNetworkLoadingStatusLoaded) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.hud.labelText = @"兑换请求发送成功!";
                    self.hud.mode = MBProgressHUDModeText;
                    [self.hud hide:YES afterDelay:3];
                    
                    [RMBManager sharedManager].user.userPoints -= self.userSelctedGift.giftCost;
                    
                    [RMBUtils increaseUserExchangeCountToday];
                    
                    [self performSegueWithIdentifier:@"ExchangeResultSegue" sender:self];
                });
                
                [[RMBManager sharedManager] fetchUserExchangeHistory:0 pageCount:5 needsTotalCount:YES];
            }
            else {
            }

        } else if (newValue == RMBNetworkLoadingStatusLoading) {
            
        }
    } else if ([keyPath isEqualToString:@"createUserLoadingStatus"]) {
        [self updateUserPointsLabel];
    }
}

-(void)updateUserPointsLabel
{
    int userPoints = [RMBManager sharedManager].user.userPoints;
    NSString *pointsString = [NSString stringWithFormat:@"%d", userPoints];
    NSString *moneyString = [NSString stringWithFormat:@"%.2f", userPoints / 100.0f];
    self.pointsLabel.text = pointsString;
    self.moneyLabel.text = moneyString;
}
@end
