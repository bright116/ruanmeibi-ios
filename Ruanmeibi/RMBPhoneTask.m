//
//  RMBPhoneTask.m
//  Ruanmeibi
//
//  Created by Liang Zhu on 14-1-27.
//  Copyright (c) 2014年 Liang Zhu. All rights reserved.
//

#import "RMBPhoneTask.h"

@implementation RMBPhoneTask

- (void)setTimeStampWithString:(NSString *)timestampString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    [dateFormatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSSZZZZZZ"];
    
    self.timeStamp = [dateFormatter dateFromString:timestampString];
}
@end
