//
//  RMBConfig.m
//  Ruanmeibi
//
//  Created by Liang Zhu on 14-2-18.
//  Copyright (c) 2014年 Liang Zhu. All rights reserved.
//

#import "RMBConfig.h"
#import "RMBOfferwall.h"

#define kSAVED_DISPLAYING_OFFERWALLS_KEY    @"savedDisplayingOfferwalls"

@interface RMBConfig ()

@property (nonatomic, strong) NSDictionary *allSupportedOfferwalls;

@end

@implementation RMBConfig

+ (instancetype)sharedConfig {
    static id _sharedInstace = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstace = [[self alloc] init];
    });
    
    return _sharedInstace;
}

- (id)init {
    if (self = [super init]) {
        
#ifdef DEBUG
        self.appInDebug = YES;
#else
        self.appInDebug = NO;
#endif
        
        self.allSupportedOfferwalls = [self readAllSupportedAdsVendors];
        
        BOOL readyForSale = [[NSUserDefaults standardUserDefaults] boolForKey:kLocal_Key_AppReadyForSale];
        if (readyForSale) {
            NSArray *offerwallIdentifiers = [[NSUserDefaults standardUserDefaults] objectForKey:kSAVED_DISPLAYING_OFFERWALLS_KEY];
            
            self.displayingOfferwalls = [self getOfferwallsFromIdentifiers:offerwallIdentifiers];
        } else {
            // 默认只显示Aarki
            self.displayingOfferwalls = [self getOfferwallsFromIdentifiers:@[@"aarki"]];
        }
    }
    
    return self;
}

// 检查服务器端的服务器配置信息
- (void)syncAppConfig
{
    // 检查已保存状态
    BOOL readyForSale = [[NSUserDefaults standardUserDefaults] boolForKey:kLocal_Key_AppReadyForSale];
    if (readyForSale && !(self.appReadyForSale)) {
        self.appReadyForSale = YES;
    }
    
    // 远程获取版本代码, 异步
    NSURL *remoteAppConfigUrl = [NSURL URLWithString:kREMOTE_APP_CONFIG_URL];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:remoteAppConfigUrl];
    [request setHTTPMethod:@"GET"];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:request queue:queue
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if ( [data length] > 0 && !error ) { // Success
             NSDictionary *appData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
             
             //如果本地未保存应用已上线记录,则同步远端版本
             NSInteger remoteVer = [appData[kRemote_Key_verOnSale] intValue];
             NSInteger localVer = kLocalVer;
             
             // 判断应用是否正在审核中
             if ( localVer > remoteVer ) {  //审核中
                 self.appReadyForSale = NO;
                 self.displayingOfferwalls = [self getOfferwallsFromIdentifiers:@[@"aarki"]];
             }
             else { // 上线啦
                 self.appReadyForSale = YES;
                 
                 // 保存到UserDefaults
                 [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kLocal_Key_AppReadyForSale];
                 [[NSUserDefaults standardUserDefaults] synchronize];
                 
                 // 判断上线积分墙列表
                 NSArray *providers = appData[kRemote_Key_adsWallProviders];
                 self.displayingOfferwalls = [self getOfferwallsFromIdentifiers:providers];

                 NSMutableArray *displayingOfferwallsKeys = [NSMutableArray array];
                 for (RMBOfferwall *offerwall in self.displayingOfferwalls) {
                     [displayingOfferwallsKeys addObject:offerwall.identifier];
                 }
                 [[NSUserDefaults standardUserDefaults] setObject:displayingOfferwallsKeys forKey:kSAVED_DISPLAYING_OFFERWALLS_KEY];
             }
         } else {
             // 网络获取失败
         }
     }]; //_sendAsynchronousRequest
}

-(NSArray *)getOfferwallsFromIdentifiers:(NSArray *)remoteOfferwallIdentifiers
{
    NSMutableArray *mergedOfferwalls = [NSMutableArray array];
    for (NSString *offerwallIdentifier in remoteOfferwallIdentifiers) {
        RMBOfferwall *offerwall = (self.allSupportedOfferwalls)[offerwallIdentifier];
        if (nil != offerwall) {
            [mergedOfferwalls addObject:offerwall];
        } else {
            DDLogVerbose(@"Unrecognized offerwall identifier %@", offerwallIdentifier);
        }
    }
    
    return mergedOfferwalls;
}

#define kLOCAL_SUPPORTED_OFFERWALLS     @"supportedAdsVendors"

// 读取本版本所有支持的积分墙
-(NSDictionary *)readAllSupportedAdsVendors
{
    NSString *path = [[NSBundle mainBundle] pathForResource:kLOCAL_SUPPORTED_OFFERWALLS ofType:@"plist"];
    NSArray *localSupportedOfferwalls = [NSArray arrayWithContentsOfFile:path];
    
    NSMutableDictionary *offerwalls = [NSMutableDictionary dictionaryWithCapacity:[localSupportedOfferwalls count]];
    for (NSDictionary *offerwallInfo in localSupportedOfferwalls) {
        NSString *nameOfClass = offerwallInfo[@"class"];
        
        RMBOfferwall *offerwall = [[NSClassFromString(nameOfClass) alloc] init];
        if (offerwall != nil) {
            offerwall._id = [offerwallInfo[@"_id"] intValue];
            offerwall.identifier = offerwallInfo[@"identifier"];
            offerwall.name = offerwallInfo[@"name"];
            offerwall.image = [UIImage imageNamed:offerwallInfo[@"icon"]];
            offerwall.description = offerwallInfo[@"description"];
            offerwall.promotion = offerwallInfo[@"promotion"];

            offerwalls[offerwall.identifier] = offerwall;
        }
    }
    
    return offerwalls;
}

@end
