//
//  HelpViewController.h
//  whoTalking
//
//  Created by  on 12-3-6.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpViewController : UIViewController<UIScrollViewDelegate>

@property (nonatomic, strong) UIScrollView *myScrollView;
//@property (nonatomic, strong) UIPageControl *myPageControl;

@end
