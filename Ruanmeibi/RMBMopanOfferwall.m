//
//  RMBMopanOfferwall.m
//  Ruanmeibi
//
//  Created by Liang on 14-4-25.
//  Copyright (c) 2014年 Liang Zhu. All rights reserved.
//

#import "RMBMopanOfferwall.h"
#import "MopanAdWall.h"
#import "RMBManager.h"

@interface RMBMopanOfferwall ()

@property (nonatomic, strong) MopanAdWall *mopanAdWallControl;

@end

@implementation RMBMopanOfferwall

- (void)initializeOfferwall
{
    self.mopanAdWallControl = [[MopanAdWall alloc] initWithMopan:kMopanAppID withAppSecret:kMopanAppSecret];
    [self.mopanAdWallControl setCustomUserID:[[RMBManager sharedManager].idfa UUIDString]];
}

- (void)showOfferwallWithController:(UIViewController *)controller
{
    self.mopanAdWallControl.rootViewController = controller;
    [self.mopanAdWallControl showAppOffers];
}

-(void) applicationDidEnterBackground
{
    // default implementation does nothing
}

-(void) applicationDidEnterForeground
{
    // default implementation does nothing
}

@end
