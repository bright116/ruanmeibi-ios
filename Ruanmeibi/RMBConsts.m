//
//  RMBConsts.m
//  Ruanmeibi
//
//  Created by Liang on 14-3-29.
//  Copyright (c) 2014年 Liang Zhu. All rights reserved.
//

#import "RMBConsts.h"

NSString * const ALERT_JAILBREAK_TITLE = @"Jailbroken Detected";
NSString * const ALERT_JAILBREAK_MESSAGE = @"抱歉,应用不支持在越狱机器上运行.\n Sorry, the app does not support running on a jailbroken device.";
NSString * const ALERT_SIMULATOR_TITLE = @"Simulator Detected";
NSString * const ALERT_SIMULATOR_MESSAGE = @"抱歉，应用不支持在模拟器上运行。请在真机上运行本应用。\nSorry, the app does not support running on a simulator. Please run the app on a real device.";

NSString * const ALERT_EXCEED_DAILY_EXCHANGE_COUNT_TITLE = @"每日兑换次数超限";
NSString * const ALERT_EXCEED_DAILY_EXCHANGE_COUNT_MESSAGE = @"抱歉, 每天最多可以兑换%d次. \n您今日已兑换%d次.";

@implementation RMBConsts
@end
