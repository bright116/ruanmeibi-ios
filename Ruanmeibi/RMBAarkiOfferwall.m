//
//  AarkiOfferwall.m
//  Ruanmeibi
//
//  Created by Liang on 14-3-29.
//  Copyright (c) 2014年 Liang Zhu. All rights reserved.
//

#import "RMBAarkiOfferwall.h"
#import "Aarki.h"
#import "AarkiContact.h"
#import <AdSupport/ASIdentifierManager.h>

@interface RMBAarkiOfferwall ()
@property (nonatomic, strong) Aarki *aarkiLoader;
@end

@implementation RMBAarkiOfferwall

- (id)init
{
    self = [super init];
    if (self) {
        self._id = 0;
        self.image = [UIImage imageNamed:@"aarki"];
        self.name = @"Aarki积分墙入口";
        self.promotion = @"最快5分钟赚2元";
    }
    
    return self;
}

// 初始化Aarki积分墙
- (void)initializeOfferwall
{
    // User ID即为IDFA值
    [AarkiContact setUserId:[[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString]];
    
    self.aarkiLoader = [[Aarki alloc] init];
    NSLog(@"Loading Aarki SDK v. %@.", [AarkiContact libraryVersion]);
}

- (void)showOfferwallWithController:(UIViewController *)controller
{
    [self.aarkiLoader showAds:AARKI_OFFERWALL_PLACEMENT_ID withParent:controller options:nil];
}

@end
