//
//  RMBUserExchangeTableCell.h
//  Ruanmeibi
//
//  Created by Liang Zhu on 14-1-26.
//  Copyright (c) 2014年 Liang Zhu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RMBUserRewardHistoryItemCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *giftNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *exchangeStartDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *exchangeCostLabel;
@property (weak, nonatomic) IBOutlet UILabel *exchangeStateLabel;

@end
