//
//  RMBReward.m
//  Ruanmeibi
//
//  Created by Liang Zhu on 14-1-26.
//  Copyright (c) 2014年 Liang Zhu. All rights reserved.
//

#import "RMBReward.h"

@implementation RMBReward

- (void)setTimeStampWithString:(NSString *)timestampString {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    [dateFormatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSSZZZZZZ"];
    
    self.timeStamp = [dateFormatter dateFromString:timestampString];
}

- (NSString *)getUserExchangeStatusString {
    // TODO: 远程获取兑换状态码和提示信息
    
    const NSDictionary *statusMap =
        @{
          @(EXCHANGE_STATUS_BEGIN)         : @"兑换中",
          @(EXCHANGE_STATUS_PROCESSING)    : @"兑换中",
          @(EXCHANGE_STATUS_FIRST_CONFIRM) : @"兑换中",
          @(EXCHANGE_STATUS_DONE)          : @"成功",
          @(EXCHANGE_STATUS_ERROR)         : @"失败"
        };
    
    return statusMap[@(self.userExchangeStatus)];
}
@end