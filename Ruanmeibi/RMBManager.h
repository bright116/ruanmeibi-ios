//
//  RMBManager.h
//  Ruanmeibi
//
//  Created by Liang Zhu on 14-1-22.
//  Copyright (c) 2014年 Liang Zhu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RMBUser.h"
#import "RMBHTTPJSONClient.h"

typedef NS_ENUM(NSInteger, RMBNetworkLoadingStatus) {
    RMBNetworkLoadingStatusUnloaded = 0,
    RMBNetworkLoadingStatusLoading  = 1,
    RMBNetworkLoadingStatusLoaded   = 2,
    RMBNetworkLoadingStatusError    = 3,
};

@interface RMBManager : NSObject

// Singleton
+ (instancetype)sharedManager;

// 创建用户
- (void)createUser;

// 获取用户状态信息
- (void)getUserStatus;

// 获取兑换物品列表
- (void)fetchGifts;

// 创建兑换请求
- (void)createGiftExchangeWithGiftId:(int)giftId andPhoneNumber:(NSString *)userEnteredNumber;

// 获取用户兑换历史
- (void)fetchUserExchangeHistory:(int)page pageCount:(int)pageCount needsTotalCount: (BOOL) needsTotalCount;

// 获取用户的任务历史
- (void)fetchUserTaskHistory:(int)page pageCount:(int)pageCount;

//获取新的完成任务总分和信息列表
-(void)getNewTask;

- (void)setServerIP:(NSString *)ipAddress andPort:(NSString *)port;

@property (nonatomic, strong) NSString *ipAddress;
@property (nonatomic, strong) NSString *port;

// 用户信息
@property (nonatomic, strong) RMBUser *user;

// 可兑换物品列表
@property (nonatomic, strong) NSArray *gifts;

// 用户兑换历史
@property (nonatomic, strong) NSMutableArray *userExchangeHistory;

// 用户任务历史
@property (nonatomic, strong) NSMutableArray *userTaskHistory;

// 用户新完成的任务
@property (nonatomic, strong) NSMutableDictionary *userNewTask;

// 用于与Berlusconi服务器端通信的客户端
@property (nonatomic, strong) RMBHTTPJSONClient *client;

@property (nonatomic) RMBNetworkLoadingStatus createUserLoadingStatus;
@property (nonatomic) RMBNetworkLoadingStatus getUserStatusLoadingStatus;
@property (nonatomic) RMBNetworkLoadingStatus giftsLoadingStatus;
@property (nonatomic) RMBNetworkLoadingStatus createUserExchangeTradeStatus;
@property (nonatomic) RMBNetworkLoadingStatus taskHistoryLoadingStatus;
@property (nonatomic) RMBNetworkLoadingStatus rewardHistoryLoadingStatus;
@property (nonatomic) RMBNetworkLoadingStatus getNewTaskLoadingStatus;

@property (nonatomic) NSUUID *idfa;

@property (nonatomic, strong) NSDate *userLatestExchangeDate;
@property (nonatomic, strong) NSNumber * userExchangeCountToday;

//判断是否是后台切入
@property (nonatomic, assign) BOOL appEnterForeground;

@end