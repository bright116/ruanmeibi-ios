//
//  SecondViewController.h
//  Ruanmeibi
//
//  Created by Liang Zhu on 1/16/14.
//  Copyright (c) 2014 Liang Zhu. All rights reserved.
//

@interface RMBGiftViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@end