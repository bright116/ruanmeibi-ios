//
//  RMBGift.m
//  Ruanmeibi
//
//  Created by Liang Zhu on 14-1-22.
//  Copyright (c) 2014年 Liang Zhu. All rights reserved.
//

#import "RMBGift.h"

@implementation RMBGift

- (RMBGift *) initWithId:(int)giftId type:(int)type cost:(int)cost counts:(long)counts exchangeRate:(float)exchangeRate name:(NSString *)name {
    self = [self init];
    if (self) {
        self.giftId = giftId;
        self.giftType = type;
        self.giftCost = cost;
        self.giftCounts = counts;
        self.giftExchangeRate = exchangeRate;
        self.giftName = [name copy];
    }
    
    return self;
}
@end
