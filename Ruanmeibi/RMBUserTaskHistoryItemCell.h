//
//  RMBUserTaskHistoryItemCell.h
//  Ruanmeibi
//
//  Created by Liang Zhu on 14-1-27.
//  Copyright (c) 2014年 Liang Zhu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RMBUserTaskHistoryItemCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *taskVendorNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *taskTimeStampLabel;
@property (weak, nonatomic) IBOutlet UILabel *taskEarnedPointsLabel;
@property (weak, nonatomic) IBOutlet UILabel *taskAppNameLabel;

@end
