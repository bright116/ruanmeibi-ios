//
//  RMBChargeStatusViewController.h
//  Ruanmeibi
//
//  Created by Liang on 14-3-28.
//  Copyright (c) 2014年 Liang Zhu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RMBChargeStatusViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *remainingExchangeCountTodayLabel;

@end
