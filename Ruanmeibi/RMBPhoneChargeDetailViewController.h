//
//  RMBPhoneChargeDetailViewController.h
//  Ruanmeibi
//
//  Created by Liang Zhu on 14-1-24.
//  Copyright (c) 2014年 Liang Zhu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RMBGift.h"

@interface RMBPhoneChargeDetailViewController : UIViewController <UITextFieldDelegate>

@property (strong, nonatomic) RMBGift *userSelctedGift;

@end
