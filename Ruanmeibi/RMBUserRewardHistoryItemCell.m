//
//  RMBUserExchangeTableCell.m
//  Ruanmeibi
//
//  Created by Liang Zhu on 14-1-26.
//  Copyright (c) 2014年 Liang Zhu. All rights reserved.
//

#import "RMBUserRewardHistoryItemCell.h"

@implementation RMBUserRewardHistoryItemCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
