//
//  RMBUtils.h
//  Ruanmeibi
//
//  Created by Liang on 14-3-27.
//  Copyright (c) 2014年 Liang Zhu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RMBUtils : NSObject
+(BOOL)isRealDevice;
+(BOOL)isDeviceJailbroken;

+(NSString *)deviceIDFA;
+(NSString *)deviceModel;
+(NSString *)deviceOS;

+(NSString *)appVersion;
+(NSString *)appBuild;

+ (BOOL)isValidateMobile:(NSString *)mobile;
+ (BOOL)isValidateQQ:(NSString *)qq;
//+(NSString *)deviceLocalIPAddress;

+ (NSString *)formattedDateTimeString:(NSDate *)date format:(NSString *)format;
+ (BOOL)isSameDay:(NSDate *)date1 withDate:(NSDate *)date2;

+ (int)readUserExchangeCountToday;
+ (void)increaseUserExchangeCountToday;
@end
