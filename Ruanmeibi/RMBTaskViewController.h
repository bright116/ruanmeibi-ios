//
//  RMBTaskViewController.h
//  Ruanmeibi
//
//  Created by Liang Zhu on 1/16/14.
//  Copyright (c) 2014 Liang Zhu. All rights reserved.
//

#import "YouMiView.h"
#import "MiidiAdWallShowAppOffersDelegate.h"


@interface RMBTaskViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@end