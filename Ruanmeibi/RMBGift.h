//
//  RMBGift.h
//  Ruanmeibi
//
//  Created by Liang Zhu on 14-1-22.
//  Copyright (c) 2014年 Liang Zhu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RMBGift : NSObject

@property int giftId;
@property int giftType;
@property int giftCost;
@property long giftCounts;
@property float giftExchangeRate;

@property (nonatomic, strong) NSString *giftName;

- (RMBGift *) initWithId:(int)giftId type:(int)type cost:(int)cost counts:(long)counts exchangeRate:(float)exchangeRate name:(NSString *)name;
@end
